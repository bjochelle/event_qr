<?php 
include 'core/config.php';

$fetch=mysql_fetch_array(mysql_query("SELECT * FROM tbl_certificate where event_id='$_GET[id]'"));?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Create Certificate </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                 <div id="createCertificate" class="container">

                   <div class="col-md-12 input-group">
                     <div class="col-md-4" >
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Font Family: <span style="color:red;">*</span></span></strong></span>
                         <input type="text" name="font_family" required="" id="font_family" value="<?php echo $fetch['name_font_family'];?>">
                      </div>
              
                     
                    </div>
                 
                  <div class="col-md-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><strong>Font size: <span style="color:red;">*</span></span></strong></span>
                      <input type="number" name="font_size" required="" min="0" step="2" id="font_size" value="<?php echo $fetch['name_font_size'];?>">
                    </div>
                    
                  </div>

                    <div class="col-md-4 input-group">
                        <button class="btn btn-primary btn-sm" onclick="updatetext()" id="btn_update"><span class="fa fa-refresh"></span> Change Font  </button>
                         <button class="btn btn-success btn-sm" onclick="saveCert()"><span class="fa fa-plus-circle" > </span> Save Certificate </button>
                    </div>

                </div>
                <br><br><br><br>
                 <div id="draggable" class="ui-widget-content" style="position: absolute;">
                    <p id="textname">Firtsname Surname</p>
                  </div>

                 
                  <span id="image"></span>
                 </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
  <style>
  #draggable { width: auto !important;border: none !important;background: none !important;cursor: grab;
    height: 50px; padding: 0.5em;text-align: center; }
  </style>
  <script>
  $( function() {
    $('#draggable').draggable({
    drag: function() {
        var offset = $(this).offset();
        var xPos = offset.left;
        var yPos = offset.top;
        // $('#posX').text('x: ' + xPos);
        // $('#posY').text('y: ' + yPos);
        // $("#textname").text('x: ' + xPos+' y:' + yPos);
    }
});

  } );
  </script>


  <script type="text/javascript">
    $(document).ready(function(){
      getImage();
    });


    function updatetext(){
      var font_size = $("#font_size").val();
      var font_family = $("#font_family").val();
        $("#textname").css({
          fontFamily: "'"+font_family+"'",
          fontSize: font_size+"px"
        });
    }
      function saveCert(){
        var event_id = '<?php echo $_GET['id'];?>';
        var font_size = $("#font_size").val();
        var font_family = $("#font_family").val();
        var offset = $("#draggable").offset();
        var xPos = offset.left;
        var yPos = offset.top;

         $.ajax({
            url:"ajax/saveCert.php",
            method:"POST",
            data:{
                event_id:event_id,
                xPos:xPos,
                yPos:yPos,
                font_size:font_size,
                font_family:font_family
            },success: function(data){
               if(data == 1){
                  success_update();
                  setTimeout(function(){
                    location.reload();
                  },3000)
                }else{
                  failed_query();
                }
            }
          });
      }

    function getImage(){
      var id = '<?php echo $_GET['id'];?>';

     $.ajax({
      url:"ajax/getCert.php",
      method:"POST",
      data:{
        id:id
      },
      success: function(data){
        var o = JSON.parse(data);
        $("#textname").css({
          fontFamily: "'"+o.font_family+"'",
          fontSize: o.font_size+"px"
        });
        var y = o.pos_y;


        $("#draggable").offset({top:o.pos_y,left:o.pos_x});
         // $("#textname").text('x: ' + o.pos_x+' y:' + o.pos_y);

        $("#image").html('<img src="images/cert/'+o.image+'" style="width: 100%;">');
      }
    });
    }



  </script>
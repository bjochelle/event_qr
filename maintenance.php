
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Teachers</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <div class="pull-right" style="padding: 20px;">
              <button class="btn btn-success btn-sm" onclick="showUploadModal()"><span class="fa fa-upload"> </span> Upload File </button>
                <button class="btn btn-warning btn-sm" id="btn_download"><span class="fa fa-download"> </span> Download Template </button>
              <button class="btn btn-primary btn-sm" onclick="showAddModal()"><span class="fa fa-plus-circle" > </span> Add Teacher </button>
          </div>
          <?php require 'modals/modal_upload_faculty.php'; ?>
          <?php require 'modals/modal_addFaculty.php';?>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Year</th>
                  <th>Section Advisory</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
              <?php require 'modals/modal_response.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">
    
$("#btn_download").click(function(e){
  e.preventDefault();
  var dl = confirm("Download Template for Teacher(csv file).");
  if(dl == true){
    window.location = 'ajax/download_template_teacher.php?bid=';
  }
  
});

    function showUploadModal(){
  $("#modalStudent").modal('show');
}

function showAddModal(){
  $("#modalAddFaculty").modal('show');
}


$("#upload_faculty").submit(function(e){
  e.preventDefault();
  
  $.ajax({
    url:"ajax/upload_template_faculty.php",
    method:"POST",
    data:new FormData(this),
    contentType:false,          // The content type used when sending data to the server.
    cache:false,                // To unable request pages to be cached
    processData:false,          // To send DOMDocument or non processed data file it is set to false
    success: function(data){
      $("#modalStudent").modal("hide");
      $("#modalResponse").modal('show');
      $("#response").html(data);
      
      get_Faculty();
    }
  });
});


  $("#add_faculty").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/add_faculty.php",
      method:"POST",
      data:$("#add_faculty").serialize(),
      success: function(data){
        if(data == 1){
          get_Faculty();
         success_add();
        }else if(data == 2){
          failed_query();
        }else{
         failed_query();
        }
        $("#modalAddFaculty").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });


  $("#update_student").submit(function(e){

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/update_student.php",
      method:"POST",
      data:$("#update_student").serialize(),
      success: function(data){
        if(data == 1){
          get_Faculty();
         success_update();
        }else if(data == 2){
          failed_query();
        }else{
          failed_query();
        }
          $("#modalUpdateStudent").modal("hide");
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });



  function get_Faculty(){
  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/faculty_dt.php",
      "dataSrc":"data"
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"year"
      },
      {
        "data":"section"
      },
      {
        "mRender": function(data,type,row){
          return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Record' value='" + row.id+ "' onclick='window.open(\"profile_teacher.php?t_id=" + row.id +"\")'><span class='fa fa-eye'></span> View Profile</button></center>";
        }
      }
    ]
  });
}
  
$(document).ready(function (){
  get_Faculty();
});
</script>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col -->


           <?php if($user_type == 'A' || $user_type == 'D'){ ?>
    <div class="col-md-12" style="margin-left: -1%;">
      <button class="btn btn-success pull-right"  data-toggle="modal" data-target="#add_event" style="margin-right: -2.5%"><span class="fa fa-fw fa-calendar-plus-o"></span> Add Event</button>

    </div>
    <?php } ?>
          <div class="col-md-12" style="margin-top: -10px;">
               <center> <label id="notif" class="col-md-8" style="margin-top: -80px;"> </label> </center>
              <div class="card card-primary">
                <div class="card-body p-0">
    
   
    <div id='calendar' style="height: 900px;"></div>
    <div id="eventContent" title="Event Details" style="display: none;">
       <div id="eventInfo"></div>
       <p><strong><a id="eventLink" target="_blank">Read More</a></strong></p>
    </div>
    
    <div class="col-md-12" style="background-color: red;">
      <div id="calendarTrash" class="calendar-trash" style="color: white;"><center><span class="fa fa-fw fa-trash"> </span> Drag events here to remove</center></div>
    </div>
  </div>

              </div>
              <!-- /.card-body -->
            </div>




            <!-- /.MODAL ADD -->
  <form action="" method="POST" id="add_event_form">
  <div id="add_event" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title"> Add Event</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Name : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="event_name" required="" placeholder="Event Name ">
                </div> <br>

                  <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Date : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="date" class="form-control" name="event_date" required="" placeholder="Event Date ">
                   </div> <br>

                   <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Time : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="time" class="form-control" name="event_time" required="" placeholder="Event Time ">
                   </div> <br>

                   <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Description : <span style="color:red;">*</span> </span>
                    </div>
                      <textarea type="text" class="form-control" name="event_description" required="" placeholder="Event Description "></textarea>
             
                </div> <br>

                    <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Place : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="event_place" required="" placeholder="Event Place ">
                </div> <br>

                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Contact Person : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="contact_person" required="" placeholder="Contact Person ">
                </div> <br>

                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Contact # : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="number" class="form-control" name="contact_num" required="" placeholder="Contact # ">
                </div> 

        </div>
        <div class="modal-footer input-group-btn">
          <span class="btn-group" role="group">
            <button type="submit" id="btn_add" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Save </button>
            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
          </span>
        </div>
      </div>
    </div>
  </div>
</form>


             <!-- /.MODAL-ADD -->


    <form action="" method="POST" id="update_form">
  <div id="view_event" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title"> View Event</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="col-md-12 input-group">
          <input type="hidden" name="event_id" id="event_id">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Name : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="event_name" id="event_name" required="" placeholder="Event Name ">
                </div> <br>

                  <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Date : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="date" class="form-control" name="event_date" id="event_date" required="" placeholder="Event Date ">
                   </div> <br>

                   <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Time : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="time" class="form-control" name="event_time" id="event_time" required="" placeholder="Event Time ">
                   </div> <br>

                   <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Description : <span style="color:red;">*</span> </span>
                    </div>
                      <textarea type="text" class="form-control" name="event_description" id="event_description" required="" placeholder="Event Description "></textarea>
             
                </div> <br>

                    <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Event Place : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="event_place" id="event_place" required="" placeholder="Event Place ">
                </div> <br>

                 <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Contact Person : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="contact_person" id="contact_person" required="" placeholder="Contact Person ">
                </div> <br>

                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Contact # : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="number" class="form-control" name="contact_num" id="contact_num" required="" placeholder="Contact # ">
                </div><br>

            <div class="col-md-12 input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">Created By : <span style="color:red;">*</span> </span>
                </div>

                <input type="text" class="form-control" id="created_by" readonly>
            </div>

        </div>
        <div class="modal-footer input-group-btn">
          <span class="btn-group" role="group">
            <button type="submit" id="btn_update" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Save Changes</button>
            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
          </span>
        </div>
      </div>
    </div>
  </div>
</form>
            <!-- /. box -->
          </div>
          <!-- /.col -->
        </div>
        
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <footer class="main-footer">
    <strong>Copyright &copy; 2018 <strong>Thesis Name</strong>.</strong>
    All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<?php 
include "core/config.php";

if($user_type == 'D'){
    $checkNumEventsSQL = mysql_query("SELECT * FROM tbl_event");
}else{
    $checkNumEventsSQL = mysql_query("SELECT * FROM tbl_event where user_id='$id'");
}

$numRowsEvents = mysql_num_rows($checkNumEventsSQL);
?> 
<!-- Page specific script -->
<script>
  $(function () {


    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    $(document).ready(function() {
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
    $('#calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },

     defaultDate: '<?php echo date('Y-m-d'); ?>',   // year-month-day
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      //Random default events
      events : [
          <?php 
          
        $ctrEvent=1;
        while($rowEvents = mysql_fetch_array($checkNumEventsSQL)){

          $userRow = mysql_fetch_array(mysql_query("SELECT fname,lname FROM `tbl_user` where user_id='$rowEvents[user_id]' "));
          $nameAdmin =$userRow['fname']." ".$userRow['lname'];

          $event_name = $rowEvents['event_name'];
          $start_date = $rowEvents['event_date'];
          $event_time = date("g:ia", strtotime($rowEvents['event_time']));
        ?>
        {
          title: '<?php echo $event_name." "."-"." ".$event_time; ?>',
          start: '<?php echo $start_date; ?>',
          color: '#3498db',
          id: '<?php echo $rowEvents['event_id']; ?>',
          event_date: '<?php echo $rowEvents['event_date']; ?>',
          event_time: '<?php echo $rowEvents['event_time']; ?>',
          event_description: '<?php echo $rowEvents['event_description']; ?>',
          event_place: '<?php echo $rowEvents['event_place']; ?>',
          contact_person: '<?php echo $rowEvents['contact_person']; ?>',
          contact_num: '<?php echo $rowEvents['contact_num']; ?>',
            created_by: '<?php echo $nameAdmin; ?>',
          event_name: '<?php echo $rowEvents['event_name']; ?>' 
        }<?php 
          if($ctrEvent < $numRowsEvents){ 
            echo ",";
          }
          $ctrEvent++;
        }
        ?>
      ],

      eventClick: function(callEvent , jsEvent, view){
        $("#view_event").modal();
        $("#event_id").val(callEvent.id);
        $("#event_name").val(callEvent.event_name);
        $("#event_date").val(callEvent.event_date);
        $("#event_time").val(callEvent.event_time);
        $("#event_place").val(callEvent.event_place);
        $("#event_description").val(callEvent.event_description);
        $("#contact_person").val(callEvent.contact_person);
        $("#contact_num").val(callEvent.contact_num);
        $("#created_by").val(callEvent.created_by);

      },
      eventDrop: function(event , delta , revertfunc){
        var title = event.title;
        var change_date = event.start.format();
        var id = event.id;

                
          $.post('ajax/eventDragDropUpdate.php', {

          change_date: change_date,
          id: id

          },
          function(data){
            if(data == 1){
              success_update();
              window.setTimeout(function(){ 
              window.location.reload();
              } ,1000);
            }else if(data == 2){
              exist();
                window.setTimeout(function(){ 
              window.location.reload();
              } ,1000);
            }else{
              window.location.reload();
            }
          });
          

      },
      eventDragStop: function(event,jsEvent) {
        var event_id = event.id;
          var trashEl = jQuery('#calendarTrash');
          var ofs = trashEl.offset();


          var x1 = ofs.left;
          var x2 = ofs.left + trashEl.outerWidth(true);
          var y1 = ofs.top;
          var y2 = ofs.top + trashEl.outerHeight(true);

          if (jsEvent.pageX >= x1 && jsEvent.pageX <= x2 &&
              jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
            
              var r = confirm("Are you sure you want to delete");
              if (r == true) {
                  $.post('ajax/delete_event.php', {
                        event_id: event_id
                      }, function(data){
                        if(data == 1){
                          success_delete();
                          window.location.reload();
                        }else if(data == 2){
                          alertMe("Unable to delete","Found important data","danger");
                        }else{
                          failed_query();
                        }
                  });
              }
                  
          }
      },
      editable  : true,
      droppable : true, // this allows things to be dropped onto the calendar !!!
      drop      : function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject')

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject)

        // assign it the date that was reported
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove()
        }

      }
    })
  });

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    //Color chooser button
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({
        'background-color': currColor,
        'border-color'    : currColor
      })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      //Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      //Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.html(val)
      $('#external-events').prepend(event)

      //Add draggable funtionality
      ini_events(event)

      //Remove event from text input
      $('#new-event').val('')
    })
  })

$(document).ready( function(){

    $("#btn_add").click(function(e){
     $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-fw fa-spinner fa-spin'></span>");
       e.preventDefault();

      var data = $("#add_event_form").serialize();
 
      $.post("ajax/addCalendarEvent.php", data, function(response){
      
        if(response == 1){
          success_add();
          location.reload();
        }else if(response  == 2){
              exist();
                window.setTimeout(function(){ 
              window.location.reload();
              } ,1000);
        }else{
          failed_query();
        }

         $("#btn_add").prop("disabled",false);
         $("#btn_add").html("<span class='fa fa-check-circle'></span> Save");
      }); 

     });

    $("#btn_update").click(function(e){

    
      e.preventDefault();
      var data = $("#update_form").serialize();

   
      $.post("ajax/update_event.php" , data , function(response){
        if(response == 1){
          $("#view_event").modal('hide');
          success_update();
          window.setTimeout(function(){ 
            location.reload();
          } ,1000);
        }else if(response == 2){
              exist();
                window.setTimeout(function(){ 
              window.location.reload();
              } ,1000);
                    
        }else{
          failed_query();
        }
      });
    });

 
    })
</script>
</body>
</html>

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2020 at 10:24 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u132092853_bcnhs`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `username`, `password`) VALUES
(1, 'Administrator', 'a', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `attendance_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_attendance`
--

INSERT INTO `tbl_attendance` (`attendance_id`, `event_id`, `user_id`, `status`, `date_added`) VALUES
(2, 3, 3, 0, '2018-08-31 12:36:14'),
(3, 3, 15, 0, '2018-08-31 12:39:58'),
(10, 2, 3, 3, '2018-09-02 10:29:48'),
(15, 3, 22, 1, '2018-09-02 10:48:20'),
(16, 3, 10, 2, '2018-09-02 10:49:30'),
(17, 3, 16, 0, '2018-09-02 10:51:42'),
(18, 3, 17, 0, '2018-09-02 10:51:47'),
(19, 7, 22, 0, '2018-09-03 09:38:57'),
(20, 7, 3, 0, '2018-09-03 11:00:04'),
(21, 10, 3, 2, '2020-04-29 14:42:45'),
(22, 16, 3, 1, '2020-04-29 14:48:43'),
(23, 15, 3, 0, '2020-04-29 16:31:50'),
(27, 14, 3, 0, '2020-05-13 14:14:34'),
(28, 12, 3, 0, '2020-05-13 14:16:00'),
(29, 30, 3, 1, '2020-05-13 14:19:25'),
(30, 29, 3, 0, '2020-05-13 14:22:36'),
(31, 11, 25, 0, '2020-08-25 10:31:24'),
(32, 32, 24, 1, '2020-08-25 13:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_certificate`
--

CREATE TABLE `tbl_certificate` (
  `cert_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `filename` text NOT NULL,
  `name_font_size` int(11) NOT NULL,
  `name_position_x` decimal(12,2) NOT NULL,
  `name_position_y` decimal(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_certificate`
--

INSERT INTO `tbl_certificate` (`cert_id`, `event_id`, `filename`, `name_font_size`, `name_position_x`, `name_position_y`) VALUES
(8, 7, 'api.PNG', 0, '0.00', '0.00'),
(9, 9, '117708464_590530688252200_7429261718028300608_o.jpg', 0, '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event`
--

CREATE TABLE `tbl_event` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(100) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` time NOT NULL,
  `event_description` varchar(100) NOT NULL,
  `event_place` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `contact_person` varchar(225) NOT NULL,
  `contact_num` varchar(225) NOT NULL,
  `qr_code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_event`
--

INSERT INTO `tbl_event` (`event_id`, `event_name`, `event_date`, `event_time`, `event_description`, `event_place`, `date`, `contact_person`, `contact_num`, `qr_code`) VALUES
(3, 'alumni  2018', '2020-11-29', '11:57:00', 'Alumni 2018', 'Fortune ', '0000-00-00', '', '', ''),
(6, 'beach Volley', '2020-11-30', '22:58:00', 'Summer time ', 'Boracay ', '0000-00-00', '', '', ''),
(7, 'Alumni', '2020-09-19', '20:00:00', 'Yearly alumni host by batch 1975', 'Business Inn', '0000-00-00', '', '', ''),
(8, 'Speaking Event With a Famous Alumni', '2020-12-24', '19:30:00', 'Better way to attend the alumni ', 'Mansilingan ', '0000-00-00', '', '', ''),
(9, ' Music Festival', '2020-10-18', '21:15:00', 'Music is life', 'California ', '0000-00-00', '', '', ''),
(10, 'Golf Tournament', '2020-11-25', '18:00:00', 'Enjoy and have fun ', 'Pannad ', '0000-00-00', '', '', ''),
(11, 'Sports Event', '2020-08-23', '09:00:00', 'Play it ', 'Panaad ', '0000-00-00', '', '', ''),
(13, 'Friendly Competition ', '2020-11-10', '20:40:00', 'Friendly night', 'Alijis Gym ', '0000-00-00', '', '', ''),
(14, 'Get Bowled Over', '2020-06-13', '13:30:00', 'play it', 'Malaysia ', '0000-00-00', '', '', ''),
(15, 'dasdas', '2020-05-08', '13:02:00', 'fdaskhh', 'hkhjk', '0000-00-00', 'khhkhk', '42374687', ''),
(16, 'sadasd', '2020-05-09', '14:02:00', 'dsfjhjh', 'hjhj', '0000-00-00', 'hkhjhk', '35246', ''),
(30, '42141', '2021-02-02', '14:01:00', 'dsadsa', 'dasd', '0000-00-00', 'sadsa', '41234', '2acceba4717d8663af037384c37384fa'),
(32, 'Special Event', '2020-08-25', '23:06:00', 'test special Event', 'sm', '0000-00-00', 'Sam Will', '09123456789', '39ab90f12d72bc60c732f2e9d86b439a');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notif`
--

CREATE TABLE `tbl_notif` (
  `notif_id` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `notif_status` varchar(1) NOT NULL,
  `view_status` int(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notif`
--

INSERT INTO `tbl_notif` (`notif_id`, `remarks`, `notif_status`, `view_status`, `date_added`) VALUES
(1, 'chen is newly Registered', 'R', 1, '2020-05-08 20:26:17'),
(2, 'Christian Sad interested in the event of dasdas', 'A', 1, '2020-04-29 16:31:50'),
(3, 'Christian Sad interested in the event of Get Bowled Over', 'A', 1, '2020-05-13 14:14:34'),
(4, 'Christian Sad interested in the event of Alumni night out ', 'A', 1, '2020-05-13 14:16:00'),
(5, 'Christian Sad interested in the event of dasd', 'A', 1, '2020-05-13 14:22:36'),
(6, 'John Doe interested in the event of Alumni', 'A', 0, '2020-08-25 10:31:24'),
(7, 'sad asd interested in the event of Special Event', 'A', 0, '2020-08-25 11:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_type` varchar(1) NOT NULL,
  `date_added` date NOT NULL,
  `address` varchar(255) NOT NULL,
  `un` varchar(100) NOT NULL,
  `pw` varchar(100) NOT NULL,
  `contact` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `filename`, `fname`, `lname`, `email`, `user_type`, `date_added`, `address`, `un`, `pw`, `contact`) VALUES
(2, 'jeongyeon-twice-uhdpaper.com-4K-5.642-wp.thumbnail.jpg', 'LJPsadas', 'Hizon dsadsa', 'JasminL@gmail.com', 'A', '2018-08-29', 'Alijis', 'a', 'a', 2147483647),
(3, 'api.PNG', 'Christian', 'Sad', 'SadChristian@Gma.com', 'S', '2018-08-29', 'talisay', 'A', 'q', 91769002),
(10, 'FB_IMG_1535833225213.jpg', 'Pauline Kaye Marie ', 'Lucasan', 'Hellokitty@gmail.com', 'S', '2018-08-30', 'Brgy alijis', 'A', '', 92648001),
(15, 'IMG_20180902_043206_654.JPG', 'Noel', 'Lipa ', 'Lnoel@Gmail.com', 'S', '2018-08-30', 'Brgy Tangub ', 'A', '', 90028810),
(16, 'IMG_20180902_043125_048.JPG', 'Pamela', 'Diez', 'Pamela1@yahoo.com', 'S', '2018-08-31', 'Brgy Estefania ', 'A', '', 91920018),
(17, 'IMG_20180902_043106_223.JPG', 'Allana ', 'Go', 'GoAllana@yahoo.com', 'S', '2018-08-31', 'Brgy alijis ', 'A', '', 92517893),
(18, 'IMG_20180902_043039_239.JPG', 'Andrey', 'Lee', 'Lee@yahoo.com', 'S', '2018-08-31', 'Brgy sum-ag', 'A', '', 92680017),
(19, 'FB_IMG_1532228002632.jpg', 'Hans', 'Tan', 'Hans@yahoo.com', 'S', '2018-08-31', 'talisay', 'A', '', 91725002),
(20, 'IMG_20180902_043016_391.JPG', 'Adrianna', 'So', 'Adrianna@Gmail.com', 'S', '2018-08-31', 'Brgy airport', 'A', '', 909120636),
(21, 'IMG_20180902_043206_654.JPG', 'Michael', 'harbers', 'Harbers@Gmail.com', 'S', '2018-09-01', 'Magsungay', 'A', '', 91779229),
(24, '', 'sad', 'asd', 'dasd@gmail.com', 'S', '2020-08-25', 'asadsa', 'w', 'w', 14),
(25, 'api.PNG', 'John', 'Doe', 'johndoe@gmail.com', 'S', '2020-08-25', 'taculing', 'ww', 'ww', 1),
(26, '', 'dsa', 'sad', 'asd@gmail.com', 'S', '2020-08-25', 'asdsa', 'www', 'wwww', 124),
(27, '', 'dasd', 'asdas', 'adasd@gmail.com', 'S', '2020-08-25', 'dsads', 'qqq', 'qqq', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`attendance_id`);

--
-- Indexes for table `tbl_certificate`
--
ALTER TABLE `tbl_certificate`
  ADD PRIMARY KEY (`cert_id`);

--
-- Indexes for table `tbl_event`
--
ALTER TABLE `tbl_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `tbl_notif`
--
ALTER TABLE `tbl_notif`
  ADD PRIMARY KEY (`notif_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_certificate`
--
ALTER TABLE `tbl_certificate`
  MODIFY `cert_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_event`
--
ALTER TABLE `tbl_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_notif`
--
ALTER TABLE `tbl_notif`
  MODIFY `notif_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

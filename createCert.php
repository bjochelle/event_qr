<style type="text/css">
  @media print {
    body {
  font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
  font-size: 1em;
  color: #333333;
  margin-top: 2cm;
  margin-right: 2cm;
  margin-bottom: 1.5cm;
  margin-left: 2cm
}

  #report{
    margin-top: 10px;
  }
        }
</style>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Create Certificate </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
               <form id="upload_image">
              <div class="row">

                <div class="col-md-12 input-group">
                  <div class="col-md-6">
                   <div class="input-group-prepend">
                      <span class="input-group-text"><strong>Event Name : <span style="color:red;">*</span></span></strong></span>
                      <select id="event_id" name="event_id" >
                      <option value="">-- Select Event --</option>
                      <?php 
                      include "core/config.php";
                       if($user_type === 'A' ){
                             $event = mysql_query("SELECT * from tbl_event  where user_id='$id'");
                       }else{
                             $event = mysql_query("SELECT * from tbl_event  ");

                       }

                 
                     while($row = mysql_fetch_array($event)){

                      $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_certificate where event_id='$row[event_id]'"));
                       if($count[0]==0){
                          ?>
                              <option value="<?php echo $row['event_id'];?>"><?php echo $row['event_name'] ?></option>

                      <?php }} ?>
                    </select>
                    </div>
                  
                    
                  </div>
                    <div class="col-md-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>File: <span style="color:red;">*</span></span></strong></span>
                        <input type="file" name="filename" required="">
                      </div>

                    </div>
                      <div class="col-md-2">
                        <button class="btn btn-primary btn-sm" onclick="upload()" id="btn_gen"><span class="fa fa-upload"></span> Upload </button>
                      </div>
                     
                </div>
                </div>
                 </form>
                 <br>
                 <hr>
                 <div id="createCertificate" style="display: none;">

                   <div class="col-md-12 input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Font Family: <span style="color:red;">*</span></span></strong></span>
                      </div>
              
                      <input type="text" name="font_family" required="" id="font_family">
                 
                    <div class="input-group-prepend">
                      <span class="input-group-text"><strong>Font size: <span style="color:red;">*</span></span></strong></span>
                    </div>
                    <input type="number" name="font_size" required="" min="0" step="2" id="font_size">

                    <div class="col-md-4 input-group">
                        <button class="btn btn-primary btn-sm" onclick="updatetext()" id="btn_update"><span class="fa fa-refresh"></span> Change Font  </button>
                         <button class="btn btn-success btn-sm" onclick="saveCert()"><span class="fa fa-plus-circle" > </span> Save Certificate </button>
                    </div>

                </div>
                <br>

                 <div id="draggable" class="ui-widget-content" style="position: absolute;">
                    <p id="textname">Firtsname Surname</p>
                  </div>

                 
                  <span id="image"></span>
                 </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
 <!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <style>
  #draggable { width: auto !important;border: none !important;background: none !important;cursor: grab;
    height: 50px; padding: 0.5em;text-align: center; }
  </style>
<!--   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
  <script>
  $( function() {
    $('#draggable').draggable({
    drag: function() {
        var offset = $(this).offset();
        var xPos = offset.left;
        var yPos = offset.top;
        // $('#posX').text('x: ' + xPos);
        // $('#posY').text('y: ' + yPos);
    }
});

  } );
  </script>


  <script type="text/javascript">
    $(document).ready(function(){
      // $("#createCertificate").hide();
    });
    $("#upload_image").submit(function(e){
  e.preventDefault();
  var event_id = $("#event_id").val();
  $.ajax({
    url:"ajax/upload_certificate.php",
    method:"POST",
    data:new FormData(this),
    contentType:false,          // The content type used when sending data to the server.
    cache:false,                // To unable request pages to be cached
    processData:false,          // To send DOMDocument or non processed data file it is set to false
    success: function(data){
       if(data == 1){
          success_add();
          getImage(event_id);
      }else if(data == 2){
        alertMe('Ooppss','Event Certificate already exist','danger');
      }else{
        window.location.reload();
      }
    }
  });
});


    function updatetext(){
      var font_size = $("#font_size").val();
      var font_family = $("#font_family").val();
        $("#textname").css({
          fontFamily: "'"+font_family+"'",
          fontSize: font_size+"px"
        });
    }
      function saveCert(){
        var event_id = $("#event_id").val();
        var font_size = $("#font_size").val();
        var font_family = $("#font_family").val();
        var offset = $("#draggable").offset();
        var xPos = offset.left;
        var yPos = offset.top;

         $.ajax({
            url:"ajax/saveCert.php",
            method:"POST",
            data:{
                event_id:event_id,
                xPos:xPos,
                yPos:yPos,
                font_size:font_size,
                font_family:font_family
            },success: function(data){
               if(data == 1){
                  success_update();
                  setTimeout(function(){
                    location.reload();
                  },3000)
                }else{
                  failed_query();
                }
            }
          });
      }

    function getImage(id){
     $.ajax({
      url:"ajax/getImage.php",
      method:"POST",
      data:{
        id:id
      },
      success: function(data){
        $("#createCertificate").css("display","block");
        $("#image").html('<img src="images/cert/'+data+'" style="width: 100%;">');
      }
    });
    }



  </script>
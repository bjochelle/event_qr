<?php 

include "core/config.php";
$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';

  session_start();
  $id= $_SESSION['id'];
   $name= $_SESSION['name'];
  $user_type= $_SESSION['user_type'];

if($id == ""){
  header("Location: index.php");
}


date_default_timezone_set("Asia/Manila");
$date_today = date('Y-m-d');

if($user_type === 'A' ){
$check_event_today= mysql_query("SELECT * FROM tbl_event where event_date = '$date_today' and  user_id='$id'");
}else{
 $check_event_today= mysql_query("SELECT * FROM tbl_event where event_date = '$date_today' "); 
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Eventia</title>

  <link rel="icon" type="image/ico" href="images/logo.png" />

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="dist/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <link rel="stylesheet" href="plugins/select2/select2.min.css">
  <link rel="stylesheet" href="assets/css/animate.css">
  <link rel="stylesheet" href="plugins/datatables/jquery.dataTables.min.css"></link>
  



  <!-- ./wrapper -->
  <script src="plugins/jquery/jquery.js"></script> 
<!-- <script src="plugins/jquery/jquery.min.js"></script>  -->

<!-- Select2 -->


<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="plugins/jquery/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- SlimScroll 1.3.0 -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<script src="dist/js/bootstrap-notify.js"></script>
<script src="dist/js/bootstrap-notify.min.js"></script>

  <!-- bootstrap time picker -->
<script src="dist/js/date-time-picker.min.js"></script>

  <script type="text/javascript">



      $(document).ready(function(){

  var user_type = '<?php echo $user_type;?>';

  // if (user_type == 'A'){
  //   var eSource,source,source1;  //define my global eventSource Object
  //     if (!!window.EventSource)  //Check for Browser feature compatibiliity
  //      {
  //       var eSource= new EventSource('sse.php');  //instantiate the Event source
  //       eSource.onmessage = function(event) {
  //         document.getElementById("count_notif").innerHTML = event.data;
  //       };

  //       var eSource= new EventSource('sse_notif.php');  //instantiate the Event source
  //       eSource.onmessage = function(event) {
  //         document.getElementById("count_new_interested").innerHTML = event.data;
  //       };

  //       var eSource= new EventSource('sse_new_member.php');  //instantiate the Event source
  //       eSource.onmessage = function(event) {
  //         document.getElementById("count_new_member").innerHTML = event.data;
  //       };

  //     } else {
  //       alert("You're browser does not support EventSource needed for this page ");
  //       // Fallback method perhaps you can use old-school XHR polling
  //     }
  // }


  // $('[data-toggle="tooltip"]').tooltip();
  $('#datepicker').dateTimePicker();
  $('#datepicker2').dateTimePicker();
  $('#datepicker3').dateTimePicker();

  var current_date = "<?php echo date("Y-m-d"); ?>";
  var min = new Date(current_date);
  var max = new Date(current_date);
  var mindate = min.setDate(min.getDate());
  var maxdate = max.setDate(max.getDate());
  $('#datepicker_min').dateTimePicker({
    mode:'date',
    limitMin: mindate
  });
  
  $('#datepicker_min1').dateTimePicker({
    mode:'date',
    limitMax: maxdate
  });

});
function checkAll(ele) {
      var checkboxes = document.getElementsByTagName('input');
      if (ele.checked) {
        for (var i = 0; i < checkboxes.length; i++) {
          if (checkboxes[i].type == 'checkbox') {
            checkboxes[i].checked = true;
            }
          }
      } else {
        for (var i = 0; i < checkboxes.length; i++) {
          //console.log(i)
          if (checkboxes[i].type == 'checkbox') {
            checkboxes[i].checked = false;
          }
        }
      }
    }

function success_add(){
  $.notify("<strong><span class='fa fa-check-circle'></span> All Good! </strong> Record was successfully added.");
}
function success_delete(){
  $.notify("<strong><span class='fa fa-check-circle'></span> All Done! </strong> Selected records were deleted.");
}
function success_update(){
  $.notify("<strong><span class='fa fa-check-circle'></span> Success! </strong> Record was updated.");
}

function success_send(){
  $.notify("<strong><span class='fa fa-check-circle'></span> Success! </strong> Email sent successfully.");
}






//dynamic alert
function alertMe(title, message, type){
  $.notify({
    title: '<strong>'+title+'</strong>',
    message: message
  },
  {
    type: type
  });
}

function failed_query(){
  $.notify({
    title: '<strong> Something is wrong!</strong>',
    message: 'Failed to execute query.'
  },
  {
    type: 'danger'
  });
}

function exist(){
  $.notify({
    title: '<strong>  Unable to add event in with the same date </strong>',
    message: 'Please double check.'
  },
  {
    type: 'danger'
  });
}

function exist_attendance(){
  $.notify({
    title: '<strong> Your attendance already added. </strong>',
    message: ''
  },
  {
    type: 'danger'
  });
}

function not_found(){
  $.notify({
    title: '<strong>QR not found in database. </strong>',
    message: ''
    
  },
  {
    type: 'danger'
  });
}


function exist_year(){
  $.notify({
    title: '<strong> Error. </strong>',
    message: 'Existing Year and Section'
  },
  {
    type: 'danger'
  });
}
</script>
  <style type="text/css">
    .sidebar-dark-primary .sidebar a{
      color: black;
    }
    .sidebar-dark-primary .sidebar a:hover{
      background: #757575 !important;
      color:white !important;
    }
    [class*=sidebar-dark] .user-panel{
        border-bottom: 2px solid #0f7b0a ;
    }
    .modal-header{
      background: #0f7b0a;
          color: white;
    }

    .close{
      color:white;
    }
    .dataTables_wrapper {
    	overflow-x: scroll;	
    }

    .modal-header {
    background: #9e9e9e;
    color: white;
    }
    .paginate_button{
      border: none !important;
      padding: 0 !important;
    }
     .select2-container--default .select2-selection--single{
    height: 38px !important;
  }
  </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom" style="background: #9E9E9E !important;">
    <!-- Left navbar links -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
      <li >
        <a class="nav-link" data-widget="pushmenu" href="home.php?page=dashboard"><span class="fa fa-bars" style="color: white;"></span></a>
      </li>

       <li>
        <a class="nav-link"  style="color:white; font-weight: bold;" href="#" >
        EVENTIA
        </a>
      </li>
    </ul>

    </div>
    <ul class="navbar-nav ml-auto">
       <?php if($user_type == 'A' || $user_type == 'D'){?>
        <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" >
          <i class="fa fa-bell-o"></i>
          <span class="badge badge-warning navbar-badge"><span id="count_notif"></span></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">Notifications</span>
          
          <div class="dropdown-divider"></div>
          <span id="notif_list"></span>
          <a href="home.php?page=new_member" class="dropdown-item">
            <span class="fa fa-users mr-2"></span>  <span id="count_new_member"> </span> new member 
          </a>
           <div class="dropdown-divider"></div>
          <span id="notif_list"></span>
          <a href="home.php?page=new_interested" class="dropdown-item">
            <i class="fa fa-calendar mr-2"></i>  <span id="count_new_interested"> </span> person interested in the event 
          </a>
          <div class="dropdown-divider"></div>
          <a href="home.php?page=all_notif" class="dropdown-item dropdown-footer">See All</a>
        </div>
      </li>
    <?php }?>
      <li class="nav-item dropdown ">
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
          <span class="fa fa-user" style="color: #ffffff !important;"></span> <span class="brand-text font-weight-light" style="color: white;
        font-size: 20px;">  <?php echo $name;?>   <span class="fa fa-caret-down"> </span>   
        </span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right ">
          <span class="dropdown-item dropdown-header">Settings</span>
          <div class="dropdown-divider"></div>
          <a href="home.php?page=profile" class="dropdown-item">
            <span class="fa fa-user mr-2"></span>  Profile
          </a>

          <div class="dropdown-divider"></div>
          <a href="ajax/logout.php" class="dropdown-item">
            <span class="fa fa-sign-out mr-2"></span> Logout
          </a>
          <div class="dropdown-divider"></div>
        </div>
      </li>
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: white">
    <!-- Brand Logo -->
    <a href="home.php?page=dashboard" class="brand-link" style="background: #9E9E9E;">
      <img src="images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light" style="color:white;"> EVENTIA </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar" style="background: #fff;">
      <!-- Sidebar user panel (optional) -->
   <!--    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="dashboard.php" class="d-block"><i class="nav-icon fa fa-dashboard"></i> Dashboard</a>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
             <li class="nav-item">
            <a href="home.php?page=dashboard" class="nav-link">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Event Calendar
              </p>
            </a>
          </li>

            <?php if($user_type == 'D'){?>
             <li class="nav-item">
            <a href="home.php?page=admin" class="nav-link">
              <i class="nav-icon fa fa-user-secret"></i>
              <p>
                Admin
              </p>
            </a>
          </li>
            <?php }?>
            <?php if($user_type == 'A' || $user_type == 'D'){?>
             
            <li class="nav-item">
            <a href="home.php?page=certificate" class="nav-link">
              <i class="nav-icon fa fa-certificate"></i>
              <p>
                Certificate
              </p>
            </a>
          </li>



          <li class="nav-item">
            <a href="home.php?page=members" class="nav-link">
              <span class="nav-icon fa fa-user"></span>
              <p>
                Members
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="home.php?page=schedule" class="nav-link">
              <i class="nav-icon fa fa-calendar"></i>
              <p>
                Schedule Events
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="home.php?page=ticket" class="nav-link">
              <i class="nav-icon fa fa-ticket"></i>
              <p>
                Ticket
              </p>
            </a>
          </li>
          <li class="nav-item">
              <a href="home.php?page=ereceipt" class="nav-link">
                  <i class="nav-icon fa fa-file"></i>
                  <p>
                      Receipt
                  </p>
              </a>
          </li>
             
          <li class="nav-item">
            <a href="home.php?page=reports" class="nav-link">
              <i class="nav-icon fa fa-bar-chart"></i>
              <p>Reports</p>
            </a>
          </li>
           <li class="nav-item">
            <a href="home.php?page=scan1" class="nav-link">
              <i class="nav-icon fa fa-qrcode"></i>
              <p>
                Scan QR
              </p>
            </a>
          </li>
             <?php }?>
          <?php if($user_type == 'S'){?>
            <li class="nav-item">
            <a href="home.php?page=profile" class="nav-link">
              <i class="nav-icon fa fa-info-circle"></i>
              <p>
                My Information
              </p>
            </a>
          </li>

         <li class="nav-item">
            <a href="home.php?page=my_attendance" class="nav-link">
              <i class="nav-icon fa fa-tasks"></i>
              <p>
               My Attendance
              </p>
            </a>
          </li>

           <li class="nav-item">
            <a href="home.php?page=scan_qr" class="nav-link">
              <i class="nav-icon fa fa-qrcode"></i>
              <p>
                Scan QR
              </p>
            </a>
          </li>

        <?php }?>

         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
    <div class="content-wrapper">
      <?php 
      if(mysql_num_rows($check_event_today)!=0 && ($user_type === 'A' || $user_type === 'D')){
        echo'<div class="col-md-12 alert alert-primary">';
          echo' We have an Event Today. Please click the event in the calendar. ';
        echo'</div>';
      }?>
      
      <?php
        if($page == 'dashboard'){
          require 'dashboard.php';
        }else if($page == 'profile'){
          require 'profile.php';
        }else if($page == 'members'){
          require 'members.php';
        }else if($page == 'schedule'){
          require 'schedule.php';
        }else if($page == 'maintenance'){
          require 'maintenance.php';
        }else if($page == 'reports'){
          require 'reports.php';
        }else if($page == 'my_attendance'){
          require 'my_attendance.php';
        }else if($page == 'new_member'){
          require 'new_member.php';
        }else if($page == 'new_interested'){
          require 'new_interested.php';
        }else if($page == 'all_notif'){
          require 'all_notif.php';
        }else if($page == 'scan_qr'){
          require 'scan_qr.php';
        }else if($page == 'certificate'){
          require 'certificate.php';
        }else if($page == 'createCert'){
          require 'createCert.php';
        }else if($page == 'editCert'){
          require 'editCert.php';
        }else if($page == 'ticket'){
          require 'ticket.php';
        }else if($page == 'admin'){
          require 'admin.php';
        }else if($page == 'ereceipt'){
          require 'ereceipt.php';
        }else if($page == 'scan1'){
          require 'scan1.php';
        }



        ?>

    </div>


</div>

<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="plugins/jQueryUI/jquery-ui.min.js"></script>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.js"></script>


<script src="plugins/select2/select2.full.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="dist/js/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>




<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

  })
</script>
</body>
</html>


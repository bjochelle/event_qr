<!DOCTYPE HTML>
<html>

<head>
    <title>Eventia</title>

  <link rel="icon" type="image/icon type" href="images/logo.png" />
  <!-- Meta-Tags -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <meta name="keywords" >
  <script>
    addEventListener("load", function () {
      setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
      window.scrollTo(0, 1);
    }
  </script>
  <!-- //Meta-Tags -->
  <!-- Stylesheets -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <link href="css/style.css" rel='stylesheet' type='text/css' />
  <link href="dist/css/animate.css" rel='stylesheet' type='text/css'/>
  <!--// Stylesheets -->
  <!--fonts-->
  <!--//fonts-->

  <style type="text/css">
    .w3ls-login label i{
          color: #ffad01  !important;
    }
    .copy-wthree p{
      color: #ffad01  !important;
    }
  </style>
</head>

<body style="background: #9E9E9E !important;">
  <div class="w3ls-login col-md-12" style="margin-top: 5%;">
    <!-- form starts here -->
    <form action="#" method="post" id="register">  

        <h2 style="font-size: 33px;color: #ffad01  !important;font-weight: bold;margin-bottom: 10px;"> Registration Form </h2>

        <div class="agile-field-txt">
        <label>
          <i class="fa fa-user" aria-hidden="true"></i> First Name :</label>
        <input type="text" name="fname" placeholder=" " required="" autocomplete="off" />
        </div>

         <div class="agile-field-txt">
        <label>
          <i class="fa fa-user" aria-hidden="true"></i> Last Name :</label>
        <input type="text" name="lname" placeholder=" " required="" autocomplete="off" />
        </div>

         <div class="agile-field-txt">
        <label>
          <i class="fa fa-map-marker" aria-hidden="true"></i> Address :</label>
        <input type="text" name="address" placeholder=" " required="" autocomplete="off" />
        </div>

         <div class="agile-field-txt"> <label> 
          <i class="fa fa-inbox"aria-hidden="true"></i> Email :</label>
          <input type="email" name="email" placeholder="" required="" autocomplete="off" /> </div>

         <div class="agile-field-txt">
        <label>
          <i class="fa fa-phone" aria-hidden="true"></i> Contact :</label>
        <input type="text" name="contact" placeholder=" " required="" autocomplete="off" />
        </div>

        <div class="agile-field-txt">
        <label>
          <i class="fa fa-user" aria-hidden="true"></i> Username :</label>
        <input type="text" name="un" placeholder=" " required="" autocomplete="off" />
        </div>
        <div class="agile-field-txt" style="">
          <label>
            <i class="fa fa-lock" aria-hidden="true"></i> Password :</label>
          <input type="password" name="pw" placeholder=" " required="" id="myInput" />
        </div>
        
        <!-- //script for show password -->
        <div class="w3ls-login  w3l-sub">
          <input type="submit" value="Register" id="btn_login" style="background: #ffad01  ;">
        </div>
      
        <div class="w3ls-login  w3l-sub animated " id="notif" style="color: #ffad01 ;"> </div>
         <p class="mb-0">
       <a href="index.php" class="text-center">I already have a membership</a>
      </p>
    </form>

  </div>
  <!-- //form ends here -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.js"></script>

<script src="plugins/jquery/jquery.min.js"></script>
  <script type="text/javascript">

    
  $("#register").submit(function(e){
    $("#notif").removeClass("shake");
    e.preventDefault();
    $.ajax({
      url:"ajax/register.php",
      method:"POST",
      data:$("#register").serialize(),
      success: function(data){
        if(data == 1){
          $("#notif").html("<span style='color:green'>Your account has been made, please verify it by clicking the verification link that has been send to your email. Thank you.</span>");
        }else if ( data ==2){
          $("#notif").html("<span style='color:red'>Username/name is already taken</span>");
          $("#notif").addClass("shake");
        }else{
          $("#notif").html("<span style='color:red'>Something went wrong. Please try again later./span>");
          $("#notif").addClass("shake");
        }
      }
    });
  });

  </script>
</body>

</html>
<style type="text/css">
  #tab .nav-link.active, .nav-pills .show>.nav-link{
    color: #fff;
    background-color: #736e6e !important;
  }
  #tab .nav-link:not(.active):hover {
    color: #736e6e !important;
}

  .card-success.card-outline{
        border-top: 3px solid #9e9e9e !important;
  }
</style>
<?php 
  include "core/config.php";

  
  $f = mysql_fetch_array(mysql_query("SELECT * FROM `tbl_user` where user_id='$id' "));
  ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-success card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <?php if($f['filename']== "" ){?>
                      <img class="profile-user-img img-fluid img-circle" src="images/logo.png"
                       alt="User profile picture">
                  <?php }else{?>
                      <img class="profile-user-img img-fluid img-circle" src="images/<?php echo $f['filename'];?>"
                       alt="User profile picture">
                  <?php }?>
              
                </div>

                <h3 class="profile-username text-center"><?php echo $f['fname']." ".$f['lname'];?></h3>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b><i class="fa fa-map-marker mr-1"></i>Address</b> <a class="float-right"><?php echo $f['address'];?></a>
                  </li>
                  <li class="list-group-item">
                    <b><i class="fa fa-envelope mr-1"></i>Email</b> <a class="float-right"><?php echo $f['email'];?></a>
                  </li>
                  <li class="list-group-item">
                    <b><i class="fa fa-mobile mr-1"></i>Contact</b> <a class="float-right"><?php echo $f['contact'];?></a>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills" id="tab">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Settings</a></li>
                  <li class="nav-item"><a class="nav-link" href="#up_image" data-toggle="tab">Update Image</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <form class="form-horizontal" method="POST" id="update_profile">
                      <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $f['user_id'];?>">
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">First Name</label>

                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" name="fname" value="<?php echo $f['fname'];?>"  placeholder="First Name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Last Name</label>

                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" name="lname" value="<?php echo $f['lname'];?>" placeholder="Last Name">
                        </div>
                      </div>
                       <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Address</label>

                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" name="address" value="<?php echo $f['address'];?>" placeholder="Address">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" name="email" value="<?php echo $f['email'];?>"  placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputName2" class="col-sm-2 control-label">Contact</label>

                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName2" name="contact" 
                          value="<?php echo $f['contact'];?>" placeholder="Name">
                        </div>
                      </div>
                    
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-success" id="btn_update">Submit</button>
                        </div>
                      </div>
                    </form>
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="up_image">
                    <!-- Post -->
                    <form class="form-horizontal" method="POST" id="upload_image">
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Upload Image</label>
                         <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $f['user_id'];?>">
                        <div class="col-sm-10">
                          <input type="file" class="form-control" id="inputName" name="filename">
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-success" id="btn_upload_image">Submit</button>
                        </div>
                      </div>
                    </form>
                    <!-- /.post -->
         
                  </div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
 <script type="text/javascript">


  $("#upload_image").submit(function(e){
  e.preventDefault();
  
  $.ajax({
    url:"ajax/upload_image.php",
    method:"POST",
    data:new FormData(this),
    contentType:false,          // The content type used when sending data to the server.
    cache:false,                // To unable request pages to be cached
    processData:false,          // To send DOMDocument or non processed data file it is set to false
    success: function(data){
      if(data == 1){
              success_update();
              window.setTimeout(function(){ 
              window.location.reload();
              } ,1000);
            }else{
              window.location.reload();
            }
    }
  });
});

  $("#update_profile").submit(function(e){

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/update_admin.php",
      type:"POST",
      data:$("#update_profile").serialize(),
      success: function(data){
          if(data == 1){
         success_update();
        }else{
          failed_query();
        }
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("Submit ");
      }
    });
  });
 </script>
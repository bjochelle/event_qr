<style type="text/css">
  #example1_paginate{
    display: none;
  }
  #example1_length{
    display: none;
  }
  .dropdown-menu{
    min-width: 22rem !important;
  }
</style>
 <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css">
  <script src="assets/js/bootstrap-multiselect.js"></script>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Certificate</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content" id="show">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <div class="pull-right" style="    padding: 20px 20px 0px;">
              <button class="btn btn-primary btn-sm" onclick="create()"><span class="fa fa-plus-circle" > </span> Create Certificate </button>
          </div>
            <?php require 'modals/modal_show_send_list.php';?>

            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Event</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">

    function create(){
       window.location.replace("home.php?page=createCert");
    }

  function getCertificate(){
  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/table_certificate.php",
      "dataSrc":"data",
      "lengthMenu": [[-1], ["All"]],

    },
    "columns":[

      {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "mRender": function(data,type,row){
          return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='Send' value='"+row.event_id+"' onclick='showSend("+row.event_id+")' id='btn_send"+row.event_id+"'><span class='fa fa-send'></span></button><button class='btn btn-default btn-sm' data-toggle='tooltip' title='Update' value='"+row.event_id+"' onclick='update("+row.event_id+")' id='btn_update"+row.event_id+"'><span class='fa fa-pencil'></span></button></center>";
        }
      }
    ]
  });
}

function showSend(event_id){
   $("#modalSend").modal("show");
    $("i").addClass('fa fa-times');
   $("#event_id").val(event_id);
     $.ajax({
      url:"ajax/getSenderPerEvent.php",
      method:"POST",
      data:{
        event_id:event_id
      },
      success: function(data){
        $("#select").html(data);


       
      }
    });


}
// function send(event_id){
//   $("#btn_send"+event_id).prop("disabled",true);
//     $("#btn_send"+event_id).html("<span class='fa fa-spin fa-spinner'></span>");
//     var user_id = 15;

//   $.ajax({
//       url:"sendCertificate.php",
//       method:"POST",
//       data:{
//         event_id:event_id,
//         user_id:user_id
//       },
//       success: function(data){
//         alert(data)
//         // if(data == 1){
//         //   get_Faculty();
//         //  success_update();
//         // }else if(data == 2){
//         //   failed_query();
//         // }else{
//         //   failed_query();
//         // }
//         $("#btn_send"+event_id).prop("disabled",false);
//         $("#btn_send"+event_id).html("<span class='fa fa-send'></span> ");
//       }
//     });
// }

function update(id){
  window.location.replace("home.php?page=editCert&id="+id);
}
$("#addsender").submit(function(e){
  e.preventDefault();
    $("#btn_add").prop("disabled",true);
  $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");

  var  user_id= $("#example-selectAllJustVisible").val()
  var  event_id= $("#event_id").val()


  $.each( user_id, function( key, value ) {
    $.ajax({
      url:"sendCertificate.php",
      method:"POST",
      data:{
        event_id:event_id,
        user_id:value
      },
      success: function(data){
        
        // if(data == 1){
        //   get_Faculty();
        //  success_update();
        // }else if(data == 2){
        //   failed_query();
        // }else{
        //   failed_query();
        // }
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-send'></span> Send ");
      }
    });
});

});

$(document).ready(function(){
  getCertificate();
})

</script>
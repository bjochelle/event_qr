﻿<!DOCTYPE HTML>
<html>

<head>
	  <title>Eventia</title>

  <link rel="icon" type="image/icon type" href="images/logo.png" />
	<!-- Meta-Tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" >
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- //Meta-Tags -->
	<!-- Stylesheets -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link href="dist/css/animate.css" rel='stylesheet' type='text/css'/>

	<!--// Stylesheets -->
	<!--fonts-->
	<!--//fonts-->

	<style type="text/css">
		.w3ls-login label i{
			    color: #ffad01  !important;
		}
		.copy-wthree p{
			color: #ffad01  !important;
		}
	</style>
</head>

<body style="background: #9E9E9E !important;">
	<div class="w3ls-login col-md-12" >
		<!-- form starts here -->

		<form action="#" method="post" id="login" style="padding:1.5vw;margin-top: 10px;">
			<center><img src="images/logo.png" style="width: 50%;"> </img></center>
			<br>
				<h2 style="font-size: 33px;color: #ffad01  !important;font-weight: bold;margin-bottom: 10px;">  Login Form </h2>
				<div class="agile-field-txt">
				<label>
					<i class="fa fa-user" aria-hidden="true"></i> Username :</label>
				<input type="text" name="username" placeholder=" " required="" autocomplete="off" />
				</div>
				<div class="agile-field-txt" style="">
					<label>
						<i class="fa fa-lock" aria-hidden="true"></i> password :</label>
					<input type="password" name="password" placeholder=" " required="" id="myInput" />
				</div>
				
				<!-- //script for show password -->
				<div class="w3ls-login  w3l-sub">
					<input type="submit" value="Login" id="btn_login" style="background: #ffad01  ;">
				</div>
			
				<div class="w3ls-login  w3l-sub animated " id="notif" style="color: #ffad01 ;"> </div>
				           <p class="mb-0">
        <a href="register.php" class="text-center">Register a new membership</a>
      </p>
			
		</form>

	</div>
	<!-- //form ends here -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript">

		
	$("#login").submit(function(e){
		$("#notif").removeClass("shake");
	  e.preventDefault();
	  $.ajax({
	    url:"ajax/login_user.php",
	    method:"POST",
	    data:$("#login").serialize(),
	    success: function(data){
	      if(data == 1){
	      	window.location.replace("home.php?page=dashboard");
	      }else if(data == 2){
	      	$("#notif").html("Account not verified");
	      	$("#notif").addClass("shake");
	      }else{
	      	$("#notif").html("Incorrect Username or Password");
	      	$("#notif").addClass("shake");
	      }
	    }
	  });
	});

	</script>
</body>

</html>
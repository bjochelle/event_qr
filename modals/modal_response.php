<div id="modalResponse" class="modal fade" role="dialog">
	<div class="modal-dialog">
	<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><span class="fa fa-exclamation-circle"></span> Response Log </h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div class="modal-body" style='max-height: 450px; overflow: auto;'>
				<div id='response'></div>
			</div>
			<div class="modal-footer">
				<center>
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" id="close"><span class="fa fa-times-circle"></span> Close</button>
				</center>
			</div>
		</div>
	</div>
</div>

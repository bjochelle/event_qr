<form action="" method="POST" id="update_student">
	<div id="modalUpdateStudent" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					
					<h4 class="modal-title"> Update Alumni</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
				<div class="col-md-12 input-group">
					<input type="hidden" class="form-control" name="user_id" required="" id="update_id">
                    <div class="input-group-prepend">
                      <span class="input-group-text"> Student Id : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="stud_id" required="" placeholder="Student Id" id="update_stud_id">
                </div> <br>
				<div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">First Name : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="fname" required="" placeholder="First Name" id="update_fname">
                </div> <br>
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Last Name : <span style="color:red;">*</span> </span>
                    </div>

                     <input type="text" class="form-control" name="lname" required="" placeholder="Last Name" id="update_lname">
                </div> <br>
				<div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Year : <span style="color:red;">*</span> </span>
                    </div>
                     <input type="text" class="form-control" name="year" required="" placeholder="Year Graduated" id="update_year">
 
                </div> <br>

                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"> Section : <span style="color:red;">*</span> </span>
                    </div>

                    <input type="text" class="form-control" name="section" required="" placeholder="Section" id="update_section">
                </div>
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="btn_update" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Update </button>
						<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</form>
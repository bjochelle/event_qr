<div id="modalResponse" class="modal fade" role="dialog">
	<div class="modal-dialog">
	<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Ticket </h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
			</div>
			<div class="modal-body" style='max-height: 450px; overflow: auto;'>
				<div class="row col-md-12" style="    border: 3px solid #000000;" id="ticket"> 
				
				</div>
				
			</div>
			<div class="modal-footer">
				<center>
                    <button type="button" class="btn btn-sm btn-primary" onclick="myFunction()"><span class="fa fa-print" ></span> Print</button>
					<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" id="close"><span class="fa fa-times-circle"></span> Close</button>
				</center>
			</div>
		</div>
	</div>
</div>

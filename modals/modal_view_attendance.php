	<div id="modalViewAttendance" class="modal fade" role="dialog">
		<div class="modal-dialog"  style="max-width: 55% !important;">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					
					<h4 class="modal-title"> View Attendance </h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">



           <table id="view_attendance" class="table table-bordered table-striped" style="width: 100%;">
                <thead>
                <tr>

                  <th>#</th>
                  <th>Event Name</th>
                  <th>Event Date & Time</th>
                  <th>Place</th>
                  <th>Date Added</th>
                  <th>Time In</th>
                  <th>Time Out</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
				
				</div>
			</div>
		</div>
	</div>
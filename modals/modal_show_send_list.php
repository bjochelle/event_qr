<form action="" method="POST" id="addsender">
	<div id="modalSend" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					
					<h4 class="modal-title"> Send Certificate</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<input type="hidden" name="event_id" id="event_id">
				<div class="col-md-12 input-group">
            <div class="input-group-prepend">
                  <span class="input-group-text">Send To : <span style="color:red;">*</span> </span>
            </div>

               <div id="select">
               </div>
          </div> 
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="btn_add" class="btn btn-sm btn-primary" onchange="sendEmail()"><span class="fa fa-send"></span> Send </button>
						<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- <form action="" method="POST" id="form_vis_logout"> -->
	<div id="modal_vis_logout" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="    background: #007bff;
    color: white;">
                        <h4 class="modal-title"> Visitor's Logout </h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					
				</div>
				<div class="modal-body">
					  <div class="card-body">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">ID # : <span style="color:red;"> *</span></span>
                             </div>
                            <input type="text" class="form-control" id="vis_name_logout" name="vis_name_logout" placeholder="Id number (Ex: vis-1)" autocomplete="off" style="width: 73%;border: 1px solid #a5adb5;box-shadow: none;">
                        </div>
            		</div>
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="vis_logout_submit" class="btn btn-sm btn-primary"><span class="fa fa-sign-out"></span> Logout</button>
					</span>
				</div>
			</div>
		</div>
	</div>
<!-- </form> -->
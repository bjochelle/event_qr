<form action="" method="POST" id="update_history">
	<div id="modalViewHistory" class="modal fade" role="dialog">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
                    <h4 class="modal-title"> Update Advisory History</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					
				</div>
				<div class="modal-body">
					  <div class="card-body">
                        <input type="hidden" class="form-control" id="view_id_history" name="id">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Year:<span style="color:red;"> *</span></span>
                             </div>
                            <input type="number" min="1975" class="form-control" id="view_year_history" name="year" placeholder="Year" autocomplete="off">
                        </div>

               			 <div class="input-group mb-3">
                		  	<div class="input-group-prepend">
                   			 	<span class="input-group-text">Section:<span style="color:red;"> *</span></span>
                 			 </div>
                 		 	<input type="number" min="0" max="99" class="form-control" id="view_section_history" name="section" placeholder="Section" autocomplete="off">
            		    </div>
            		     
            		</div>
				</div>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						<button type="submit" id="btn_update" class="btn btn-sm btn-primary"><span class="fa fa-check-circle"></span> Save</button>
						<button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
					</span>
				</div>
			</div>
		</div>
	</div>
</form>
<?php 
//include "core/config.php";
?>
<style type="text/css">
  .datepicker-inline{
    display: none !important;
  }
   #map {
        height: 600px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
  .gm-style-iw {
    width: 300px; 
    min-height: 100px;
  }
</style>
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Map</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Map </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  

       <section class="content">
      <div class="container-fluid">
        <div class="row">


        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <section class="col-lg-12">
            <div id="map" style="">
            </div>
          </section>
        </div>
      </div>
    </section>
    <script type="text/javascript">
      function initMap() {
       var locations = [<?php 
                        $getAlllocations = $mysqli->query("SELECT * FROM `tbl_company`");
                        while($fetchrow = $getAlllocations->fetch_array(MYSQLI_ASSOC)){
                            $response = array();
                            $response[] = "<strong>Location: </strong>".$fetchrow["comm_addr"]."<br><br>"."<strong>Business Name: </strong> ".$fetchrow["commercial_name"];
                            $response[] = $fetchrow["comm_latitude"];
                            $response[] = $fetchrow["comm_longitude"];
                            echo json_encode($response).",";
                        }
                        ?>];
    //alert(locations);
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: new google.maps.LatLng(10.7325,123.0114),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
}
    </script>
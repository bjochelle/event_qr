<style type="text/css">
    @media print {
        body {
            font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
            font-size: 1em;
            color: #333333;
            margin-top: 2cm;
            margin-right: 2cm;
            margin-bottom: 1.5cm;
            margin-left: 2cm
        }

        #ticket{
            margin-top: 10px;
            width: 400px !important;
        }
        .logo{
          height:200px !important;
          width:200px !important;
        }
    }
</style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">My Attendance</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <input type="hidden" name="user_id" id="user_id" value="<?php echo $id;?>">
           <?php require 'modals/modal_view_ticket.php';?>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Event Name</th>
                  <th>Place</th>
                  <th>Status</th>
                  <th>Date Added</th>
                  <th>Time In</th>
                  <th>Time Out</th>
                  <th>Scanned By</th>

                  <th>Ticket</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

    <script src="dist/js/jquery.PrintArea.js"></script>
  <script type="text/javascript">

      function myFunction() {
          var mode = 'iframe'; // popup
          var close = mode == "popup";
          var options = { mode : mode, popClose : close};
          $("#ticket").printArea( options );

      }

  function get_attendance(){


     var user_id = $("#user_id").val();

  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/get_my_attendance.php",
      "dataSrc":"data",
      "data":{
        user_id:user_id
      }
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"place"
      },
      {
        "data":"status"
      },
      {
        "data":"date"
      },
      {
        "data":"time_in"
      },
      {
        "data":"time_out"
      },
      {
        "data":"scan"
      },
       {
        "mRender": function(data,type,row){
          return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Ticket' value='" + row.id+ "' onclick='viewTicket("+ row.id+ ")'><span class='fa fa-eye'></span> View Ticket </button></center>";
        }
      }
    ]
  });
}

function viewTicket(id){
  $("#modalResponse").modal('show');
   $.ajax({
        url:"ajax/getTicket.php",
        method:"POST",
        data:{
            id:id
        },
        success: function(data){

          console.log(data)
            $("#ticket").html(data);

        }
    });
  
}
  
$(document).ready(function (){
  get_attendance();
});
</script>
<?php
include "../core/config.php";

$image = $_POST['image'];

list($type, $image) = explode(';',$image);
list(, $image) = explode(',',$image);

$image = base64_decode($image);
$image_name = time().'.png';

// Load GD resource from binary data
$im = imageCreateFromString($image);

require "../vendor/autoload.php";
$qrcode = new QrReader($im['tmp_name']);
$text = $qrcode->text();

echo $text;
?>
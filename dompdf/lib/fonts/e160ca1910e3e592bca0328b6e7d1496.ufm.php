<?php return array (
  'codeToName' => 
  array (
    13 => 'CR',
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    164 => 'currency',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    172 => 'logicalnot',
    174 => 'registered',
    175 => 'macron',
    176 => 'degree',
    177 => 'plusminus',
    180 => 'acute',
    182 => 'paragraph',
    183 => 'periodcentered',
    184 => 'cedilla',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
    305 => 'dotlessi',
    338 => 'OE',
    339 => 'oe',
    710 => 'circumflex',
    730 => 'ring',
    732 => 'tilde',
    8211 => 'endash',
    8212 => 'emdash',
    8216 => 'quoteleft',
    8217 => 'quoteright',
    8218 => 'quotesinglbase',
    8220 => 'quotedblleft',
    8221 => 'quotedblright',
    8222 => 'quotedblbase',
    8226 => 'bullet',
    8230 => 'ellipsis',
    8242 => 'minute',
    8243 => 'second',
    8249 => 'guilsinglleft',
    8250 => 'guilsinglright',
    8260 => 'fraction',
    8364 => 'Euro',
    8482 => 'trademark',
    8722 => 'minus',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Lobster',
  'FullName' => 'Lobster Regular',
  'Version' => 'Version 2.100',
  'PostScriptName' => 'Lobster-Regular',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '50',
  'UnderlinePosition' => '-75',
  'FontHeightOffset' => '0',
  'Ascender' => '1000',
  'Descender' => '-250',
  'FontBBox' => 
  array (
    0 => '-460',
    1 => '-490',
    2 => '1549',
    3 => '1000',
  ),
  'StartCharMetrics' => '326',
  'C' => 
  array (
    0 => 600,
    13 => 600,
    32 => 217,
    33 => 257,
    34 => 353,
    35 => 659,
    36 => 548,
    37 => 796,
    38 => 682,
    39 => 196,
    40 => 437,
    41 => 413,
    42 => 515,
    43 => 442,
    44 => 216,
    45 => 290,
    46 => 263,
    47 => 311,
    48 => 546,
    49 => 317,
    50 => 492,
    51 => 514,
    52 => 476,
    53 => 517,
    54 => 500,
    55 => 464,
    56 => 499,
    57 => 489,
    58 => 267,
    59 => 276,
    60 => 358,
    61 => 515,
    62 => 362,
    63 => 441,
    64 => 688,
    65 => 668,
    66 => 641,
    67 => 497,
    68 => 663,
    69 => 459,
    70 => 459,
    71 => 578,
    72 => 665,
    73 => 375,
    74 => 536,
    75 => 601,
    76 => 379,
    77 => 787,
    78 => 668,
    79 => 665,
    80 => 573,
    81 => 665,
    82 => 601,
    83 => 570,
    84 => 481,
    85 => 716,
    86 => 615,
    87 => 962,
    88 => 620,
    89 => 620,
    90 => 542,
    91 => 400,
    92 => 362,
    93 => 400,
    94 => 421,
    95 => 444,
    96 => 201,
    97 => 526,
    98 => 444,
    99 => 384,
    100 => 525,
    101 => 384,
    102 => 267,
    103 => 507,
    104 => 505,
    105 => 267,
    106 => 240,
    107 => 518,
    108 => 267,
    109 => 752,
    110 => 505,
    111 => 439,
    112 => 468,
    113 => 486,
    114 => 376,
    115 => 382,
    116 => 267,
    117 => 505,
    118 => 414,
    119 => 661,
    120 => 522,
    121 => 486,
    122 => 428,
    123 => 352,
    124 => 532,
    125 => 352,
    126 => 431,
    160 => 217,
    161 => 219,
    162 => 384,
    163 => 455,
    164 => 500,
    165 => 558,
    166 => 532,
    167 => 542,
    168 => 392,
    169 => 587,
    170 => 345,
    171 => 589,
    172 => 444,
    173 => 290,
    174 => 553,
    175 => 446,
    176 => 314,
    177 => 479,
    178 => 328,
    179 => 336,
    180 => 316,
    181 => 505,
    182 => 542,
    183 => 434,
    184 => 227,
    185 => 229,
    186 => 329,
    187 => 586,
    188 => 749,
    189 => 745,
    190 => 836,
    191 => 439,
    192 => 668,
    193 => 668,
    194 => 668,
    195 => 668,
    196 => 668,
    197 => 668,
    198 => 764,
    199 => 497,
    200 => 459,
    201 => 459,
    202 => 459,
    203 => 459,
    204 => 375,
    205 => 375,
    206 => 375,
    207 => 375,
    208 => 663,
    209 => 686,
    210 => 665,
    211 => 665,
    212 => 665,
    213 => 665,
    214 => 665,
    215 => 414,
    216 => 658,
    217 => 716,
    218 => 716,
    219 => 716,
    220 => 716,
    221 => 620,
    222 => 583,
    223 => 480,
    224 => 526,
    225 => 526,
    226 => 526,
    227 => 526,
    228 => 526,
    229 => 526,
    230 => 650,
    231 => 384,
    232 => 384,
    233 => 384,
    234 => 384,
    235 => 384,
    236 => 267,
    237 => 267,
    238 => 267,
    239 => 267,
    240 => 526,
    241 => 505,
    242 => 433,
    243 => 439,
    244 => 433,
    245 => 433,
    246 => 433,
    247 => 415,
    248 => 428,
    249 => 505,
    250 => 505,
    251 => 505,
    252 => 505,
    253 => 505,
    254 => 473,
    255 => 505,
    305 => 267,
    338 => 846,
    339 => 663,
    699 => 241,
    700 => 228,
    710 => 293,
    730 => 156,
    732 => 403,
    8211 => 352,
    8212 => 444,
    8216 => 209,
    8217 => 238,
    8218 => 220,
    8220 => 429,
    8221 => 426,
    8222 => 440,
    8226 => 434,
    8230 => 702,
    8242 => 196,
    8243 => 341,
    8249 => 359,
    8250 => 344,
    8260 => 248,
    8308 => 352,
    8364 => 507,
    8482 => 379,
    8722 => 471,
    8725 => 488,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt0FevUEUYBdD90XvvvffeexcEpKhYYtQooHQUULp/VKqFF6RIFRAVQu4DIVwg4cLFsFYyZ2bOtD2TSmNaNDryuJa5m//ydw2siTW5Uu2qfa7XsPxVnXK/WuR0zuRszuV8fsmv+S2/50JuV9WYGlWj809NSqt0Sud0Tff0Sd/0S/8MzbAMz8iMyphMybRMz4zMzKzMz4IszKIsqTa5UW1rTrWspVmaNXk36/Je3s8H+TBb8nm+yJf5Kl9nW/ZkX/bnuxzIwRzJ0RzL8fxUrWpWta5xL3TDZ6p+uVf9a3ANqCE1u6bWkpqWn6tLja/ONb2W1cwamz9yqRbWhJqSm7UoF3OquubPXM21/Jt2aZ026ZC2aZ+O6ZLe6ZGe6ZUhGZhBGZxuGZ0JGZtxmZTxNSITMy+zMydzszhT831WZlmW552syKqsztpszoZszKZ8lo/zST7N+mzNjmzPN9mVb2tkduZwDuWH/JgT2ZuTL/8Cz/XRS+8wObubIAe8IWpBzX/K38UvuHr5w7KiaRMBAAAAAAAAAAAAAAAAAPB/Uh2rQ0Ord/Wpvg/rntWrejQM32qo7zSyem7Ne6zXrbo/ZdKVJoramMuveP8n1KDXe15zqhnNnYBXqYY/+g5t7hwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwNvhAUjPRdg=',
  '_version_' => 6,
);
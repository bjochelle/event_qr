<!-- <link rel="stylesheet" href=assets/main.css>
<link rel="stylesheet" href=assets/custom.css>
<link rel="stylesheet" href=assets/css/sharebar.css> -->
 <script src="assets/js/jquery.js"></script>
</head>
 <body data-instant-allow-query-string data-instant-allow-external-links>
 
   <main class="default-content" aria-label="Content">
     <div class="wrapper-content">
       <style>
#qr {
    width: 640px;
    border: 1px solid silver
}
@media(max-width: 600px) {
    #qr {
        width: 300px;
        border: 1px solid silver
    }
}
button:disabled,
button[disabled]{
  opacity: 0.5;
}
.scan-type-region {
    display: block;
    border: 1px solid silver;
    padding: 10px;
    margin: 5px;
    border-radius: 5px;
}
.scan-type-region.disabled {
    opacity: 0.5;
}
.empty {
    display: block;
    width: 100%;
    height: 20px;
}
#qr .placeholder {
    padding: 50px;
}
</style>
  <h3 class="profile-username text-center" id="msg">  </h3>
<div style="width: 500px" id="reader"></div>


<script src="assets/js/html5-qrcode.min.js"></script>
<script type="text/javascript">

function onScanSuccess(qrCodeMessage) {

    $.ajax({
              url: "ajax/decode_qr1.php",
              type: "POST",
              data: {"code":qrCodeMessage},
              success: function (data) {
                html5QrcodeScanner.clear();

                $("#confirm_attendance").show();
             
                  $("#msg").html(data);
                  $("#qrscanner").css("display","none");

                  setTimeout(function(){
                    location.reload();
                  },2000)

              }
            });
    // handle on success condition with the decoded message
}

var html5QrcodeScanner = new Html5QrcodeScanner(
    "reader", { fps: 10, qrbox: 250 });
html5QrcodeScanner.render(onScanSuccess);
</script>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <span class='fa fa-bullhorn'> </span> Member/s Notification</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content" id="show">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
              <div class="card-header">
                <h3 class="card-title">Members</h3>
              </div>
              <div class="card-body table-responsive pad">

                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <th>New Member</th>
                    </tr>

                    <?php 
                    include "core/config.php";
                    $fetch_new=mysql_query("SELECT * FROM tbl_notif where notif_status='R' and view_status=0 ORDER BY `tbl_notif`.`notif_id` DESC");
                    while ($row = mysql_fetch_array($fetch_new)) {
                     echo '<tr style="background: #ddf6ff;">';
                    echo '  <td>';
                    echo "<span class='fa fa-user'> </span> ".ucwords($row['remarks'])."<span class='pull-right'>".date('M d, Y h:i a',strtotime($row['date_added']))."</span>";
                    echo '  </td>';
                    echo '</tr>';
                  }?>
                  
                  <!-- /.success -->
                </tbody></table>
                <br>
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <th>Old Member</th>
                    </tr>

                    <?php 
                    include "core/config.php";
                    $fetch_new=mysql_query("SELECT * FROM tbl_notif where notif_status='R' and view_status=1 ORDER BY `tbl_notif`.`notif_id` DESC");
                    while ($row = mysql_fetch_array($fetch_new)) {
                    echo '<tr>';
                    echo '  <td>';
                    echo "<span class='fa fa-user'> </span> ".ucwords($row['remarks'])."<span class='pull-right'>".date('M d, Y h:i a',strtotime($row['date_added']))."</span>";
                    echo '  </td>';
                    echo '</tr>';
                  }?>
                  
                  <!-- /.success -->
                </tbody></table>

              </div>
            </div>


            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>
  <script type="text/javascript">
    $(document).ready(function(){

      var val = 0;
        $.ajax({
          type:"POST",
          url:"ajax/update_notif.php",
          data:{
            val:val
          },
          success: function(data){
          }
        });
    })
  </script>
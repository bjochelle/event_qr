<style type="text/css">
  #example1_paginate{
    display: none;
  }
  #example1_length{
    display: none;
  }
</style>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Members</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content" id="show">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <div class="pull-right" style="    padding: 20px 20px 0px;">
           <!--    <button class="btn btn-primary btn-sm" onclick="showAddModal()"><span class="fa fa-plus-circle" > </span>  --><!-- Add Alumni </button>
              <button class="btn btn-danger btn-sm" onclick="deleteStudent()" id='btn_delete'><span class="fa fa-trash-o"> </span> Delete Alumni </button> -->
          </div>
          
          <?php require 'modals/modal_add_student.php';?>
          <?php require 'modals/modal_update_student.php'; ?>
           <?php require 'modals/modal_view_student.php'; ?>
            <?php require 'modals/modal_view_attendance.php'; ?>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
              <?php require 'modals/modal_response.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">
    
$("#btn_download").click(function(e){
  e.preventDefault();
  var dl = confirm("Download Template for Alumni(csv file).");
  if(dl == true){
    window.location = 'ajax/download_template.php?bid=';
  }
  
});

    function showUploadModal(){
  $("#modalStudent").modal('show');
}

function showAddModal(){
  $("#modalAddStudent").modal('show');
}

function getSection(){
  var sy = $("#sy").val();
   $.post("ajax/getSection.php", {
                sy: sy
            },
            function (data) {
              $("#section").html(data);
     });
}


function update(id){
        $.post("ajax/getStudent.php", {
                id: id
            },
            function (data, status) {
                var o = JSON.parse(data);
          $("#update_stud_id").val(o.stud_id);
          $("#update_fname").val(o.fname);
          $("#update_lname").val(o.lname);
          $("#update_year").val(o.year);
          $("#update_section").val(o.section);
          $("#update_id").val(o.user_id);
     });
  $("#modalUpdateStudent").modal("show");
}

function viewProf(id){
        $.post("ajax/getStudent.php", {
                id: id
            },
            function (data, status) {
                var o = JSON.parse(data);
                var teacher_id = o.t_id;


                 load(teacher_id);
          $("#view_stud_id").val(o.stud_id);
          $("#view_fname").val(o.fname);
          $("#view_lname").val(o.lname);
          $("#view_year").val(o.year);
          $("#view_section").val(o.section);
          $("#view_add").val(o.address);
          $("#view_contact").val(o.contact);
          $("#view_email").val(o.email);
          $("#view_id").val(o.user_id);
     });
  $("#modalViewStudent").modal("show");
}

function load(id){
  $.post("ajax/getFaculty.php", {
                id: id
            },
            function (data, status) {
                var o = JSON.parse(data);
          $("#view_teacher").val(o.t_fname +" "+o.t_lname);
     });
}
function viewAtt(id){

  var stud_id = id;


  var table = $('#view_attendance').DataTable();
  table.destroy();
  $("#view_attendance").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/table_view_attendance.php",
      "dataSrc":"data",
      "data":{stud_id:stud_id}
    },
    "columns":[
      {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"datetime"
      },
      {
        "data":"place"
      },
      {
        "data":"date_added"
      },
      {
        "data":"time_in"
      },
      {
        "data":"time_out"
      }
    ]
  });
  $("#modalViewAttendance").modal("show");
}
$("#upload_student").submit(function(e){
  e.preventDefault();
  
  $.ajax({
    url:"ajax/upload_template.php",
    method:"POST",
    data:new FormData(this),
    contentType:false,          // The content type used when sending data to the server.
    cache:false,                // To unable request pages to be cached
    processData:false,          // To send DOMDocument or non processed data file it is set to false
    success: function(data){
      $("#modalStudent").modal("hide");
      $("#modalResponse").modal('show');
      $("#response").html(data);
      
      get_Student();
    }
  });
});


  $("#add_student").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/add_student.php",
      method:"POST",
      data:$("#add_student").serialize(),
      success: function(data){

        if(data == 1){
          get_Student();
         success_add();
         $("#add_student")[0].reset();

        }else if(data == 2){
          exist();
        }else if(data == 3){
          no_teacher();
        }else{

         failed_query();
        }
        $("#modalAddStudent").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });


  $("#update_student").submit(function(e){

    $("#btn_update").prop("disabled",true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/update_student.php",
      method:"POST",
      data:$("#update_student").serialize(),
      success: function(data){
          if(data == 1){
          get_Student();
         success_update();
        }else if(data == 2){
          exist();
        }else if(data == 3){
          no_teacher();
        }else{
          failed_query();
        }
          $("#modalUpdateStudent").modal("hide");
        $("#btn_update").prop("disabled",false);
        $("#btn_update").html("<span class='fa fa-check-circle'></span> Update ");
      }
    });
  });

  function deleteStudent(){

   var checkedValues =  $("input[name='delete_student']:checked").map(function(){
    return this.value;
  }).get();
    
  id = [];
  if(checkedValues == ""){
    alertMe("Aw Snap!","No selected Alumni","warning");
  }else{
    var retVal = confirm("Are you sure you want to delete?");
    if( retVal == true ){
      $("#btn_delete").prop("disabled", true);
      $("#btn_delete").html("<span class='fa fa-spin fa-spinner'></span> Loading");
      $.post("ajax/delete_student.php", {
        id: checkedValues
      },
      function (data, status) {
        
      $("#response").html(data);
      $("#modalResponse").modal('show');
        get_Student();
      $("#btn_delete").prop("disabled", false);
      $("#btn_delete").html("<span class='fa fa-trash-o'></span> Delete Student");
      }

    );
    }

  }
  
}


  function get_Student(){
  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/student_dt.php",
      "dataSrc":"data",
      "lengthMenu": [[-1], ["All"]],

    },
    "columns":[
      // {
      //   "mRender": function(data,type,row){

      //     if (row.allow==1){
      //         return "<center><input type='checkbox' value='" + row.id+ "' name='delete_student'></center>";
      //     }else{
      //         return "";
      //     }
          

      //   }
      // },
      {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"address"
      },
      {
        "data":"email"
      },
      {
        "mRender": function(data,type,row){
          return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Record' value='" + row.id+ "' onclick='viewProf(" + row.id+")'><span class='fa fa-eye'></span></button><button class='btn btn-default btn-sm' data-toggle='tooltip' title='View Attendance' value='" + row.id+ "' onclick='viewAtt(" + row.id+")'><span class='fa fa-calendar'></span></button></center>";
        }
      }
    ]
  });
}

$(document).ready(function(){
  get_Student();
})

</script>
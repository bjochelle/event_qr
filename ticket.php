<style type="text/css">
  @media print {

  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }

    body {
      font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
      font-size: 1em;
      color: #333333;
      margin-top: 2cm;
      margin-right: 2cm;
      margin-bottom: 1.5cm;
      margin-left: 2cm
    }

    .select2-container--default .select2-selection--single{
      height: 38px !important;
    }

    #report{
      margin-top: 10px;
    }

    #selection{
      display: none;
    }

    img{
      width: 20%;
    }

  }



</style>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Generate Ticket </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="col-md-12 input-group" id="selection">
                    <div class="col-md-2"  style="padding: 0px;">
                      <span class="form-control "><strong>Event Name: <span style="color:red;">*</span></span></strong></span>
                    </div>
                    <div class="col-md-3" style="padding: 0px;">
                      <select class="form-control select2" style="width: 100%;" id="event_name">
                        <?php 
                          include "core/config.php";
                            if($user_type === 'A'){
                                   $event = mysql_query("SELECT * from tbl_event  where user_id='$id'");
                            }else{
                                   $event = mysql_query("SELECT * from tbl_event ");
                            }
                   
                            while($row = mysql_fetch_array($event)){ ?>
                              <option value="<?php echo $row['event_id'];?>"><?php echo $row['event_name'] ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <button class="btn btn-primary btn-sm" onclick="gen()" id="btn_gen"><span class="fa fa-refresh"></span> Generate </button>
                      <button class="btn btn-default btn-sm"  onclick="window.print()" ><span class="fa fa-print"></span> Print </button>
                      </div>
                </div>
             </div>
              <div class="card-body" id="report" style="padding: 20px;">
          
                

              </div>
        <!-- /.row -->
        <!-- Main row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  
  <script src="dist/js/jquery.PrintArea.js"></script>

  <script type="text/javascript">
    function gen() {
        var event_id = $("#event_name").val();

    if (event_id == ""){
      alert ("Please fill in the form");
    }else{

      $("#btn_gen").prop('disabled', true);
      $("#btn_gen").html("<span class='fa fa-spinner fa-spin'></span> Loading ...");

     $.ajax({
        type:"POST",
        url:"ajax/gen_ticket.php",
        data:{
          event_id:event_id
        },
        success:function(data){
             $("#report").html(data);
      
          $("#btn_gen").prop('disabled', false);
          $("#btn_gen").html("<span class='fa fa-refresh'></span> Generate");
        }
      });
      }
     
    }


function myFunction()
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(document.getElementById('report').innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}

  </script>
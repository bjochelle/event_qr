<style type="text/css">
  .card-success.card-outline{
        border-top: 3px solid #9e9e9e !important;
  }
</style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
            <h1>Event Calendar</h1>
          </div>
          <div class="col-sm-6">
            <center id="notif"> </center>
          </div>
          <div class="col-sm-3">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Event Calendar</li>

            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col -->
          
          <div class="col-md-4">
                <div class="card card-success card-outline">
              <div class="card-body box-profile">

                 <?php include "core/config.php";

              if($user_type === 'A' ){
                 $max = mysql_fetch_array(mysql_query("SELECT max(event_date) FROM `tbl_event` where user_id='$id' ORDER BY `tbl_event`.`event_date` "));
                $min = mysql_fetch_array(mysql_query("SELECT min(event_date) FROM `tbl_event` where user_id='$id' ORDER BY `tbl_event`.`event_date` "));

              }else{

                 $max = mysql_fetch_array(mysql_query("SELECT max(event_date) FROM `tbl_event` ORDER BY `tbl_event`.`event_date` "));
                $min = mysql_fetch_array(mysql_query("SELECT min(event_date) FROM `tbl_event`  ORDER BY `tbl_event`.`event_date` "));
              }
               

                echo "<input type='hidden' id='max_date' value='$max[0]'>";
                echo "<input type='hidden' id='min_date' value='$min[0]'>";
                ?>

                <input type="hidden" name="event_date" id="event_date_now" value="">
                <input type="hidden" name="event_id" id="event_id" value="">
                <input type="hidden" name="user_id" id="user_id" value="<?php echo $id;?>">
                 <input type="hidden" name="date_now" id="date_now" value="<?php echo date("Y-m-d");?>">


                <h3 class="profile-username text-center"><span class="fa fa-arrow-left pull-left" id="btn_prev" style="color: #5d5c5c;" onclick="prev()"> </span> Upcoming Event  <span class="fa fa-arrow-right pull-right" style="color: #5d5c5c;" id="btn_next" onclick="next()" > </span> </h3>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Event Name</b> <a class="float-right"> <span id="event_name"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Date </b> <a class="float-right"><span id="event_date"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Time </b> <a class="float-right"><span id="event_time"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Place</b> <a class="float-right"><span id="event_place"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Description</b> <a class="float-right"><span id="event_description"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Contact Person</b> <a class="float-right"><span id="contact_person"></span></a>
                  </li>
                  <li class="list-group-item">
                    <b>Contact No.</b> <a class="float-right"><span id="contact_num"></span></a>
                  </li>
                  <?php 

                  if($user_type == "S"){?>
                  <li class="list-group-item" id="list_btn">
                      <button class="btn btn-success btn-sm pull-right" id="btn_present" onclick="present()" id='btn_delete'><span class="fa fa-check"> </span> I'm Going </button>
                  </li>
                <?php }?>
                </ul>
              </div>
 
              <!-- /.card-body -->
            </div>
            <!-- /. box -->
          </div>
      

          <div class="col-md-8">
            <div class="card card-primary">
              <div class="card-body p-0">
                <!-- THE CALENDAR -->
                <div id="calendar"></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /. box -->
          </div>

          <div id="result"></div>
          <!-- /.col -->
        </div>

  <div id="view_event" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Confirmation</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <input type="hidden" id="event_id1">
           <center> <h5> EVENT TODAY </h5>
                <h3 id="event_name1" style="text-transform: capitalize;"> </h3>
          </center>
            <span style="color:red">By clicking "Continue", your session will be log out and it will redirect to event page. </span>
        </div>
        <div class="modal-footer input-group-btn">
          <span class="btn-group" role="group">
            <button type="button" id="btn_redirect" class="btn btn-sm btn-primary" onclick="redirect()"><span class="fa fa-arrow-right"></span> Continue</button>
            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><span class="fa fa-times-circle"></span> Close</button>
          </span>
        </div>
      </div>
    </div>
  </div>
        
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

<?php 
include "core/config.php";
if($user_type === 'A' ){
  $checkNumEventsSQL = mysql_query("SELECT * FROM tbl_event where user_id='$id'");
}else{
  $checkNumEventsSQL = mysql_query("SELECT * FROM tbl_event ");
}

$numRowsEvents = mysql_num_rows($checkNumEventsSQL);
?> 
<!-- Page specific script -->
<script>
  $(function () {


    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    $(document).ready(function() {
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
    $('#calendar').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },

     defaultDate: '<?php echo date('Y-m-d'); ?>',   // year-month-day
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      //Random default events
      events : [
          <?php 
          
        $ctrEvent=1;
        while($rowEvents = mysql_fetch_array($checkNumEventsSQL)){
          $event_name = $rowEvents['event_name'];
          $start_date = $rowEvents['event_date'];
          $event_time = date("g:i a", strtotime($rowEvents['event_time']));

          if($date_today == $start_date){
            $allow_modal = 1;
          }else{
            $allow_modal = 0;
          }
        ?>
        {
          title: '<?php echo $event_name." "."-"." ".$event_time; ?>',
          start: '<?php echo $start_date; ?>',
          color: '#3498db',
          id: '<?php echo $rowEvents['event_id']; ?>',
          event_date: '<?php echo $rowEvents['event_date']; ?>',
          event_time: '<?php echo $rowEvents['event_time']; ?>',
          event_description: '<?php echo $rowEvents['event_description']; ?>',
          event_place: '<?php echo $rowEvents['event_place']; ?>',
          allow_modal: '<?php echo $allow_modal; ?>',
          user_type: '<?php echo $user_type; ?>',
          event_name: '<?php echo $rowEvents['event_name']; ?>' 
        }<?php 
          if($ctrEvent < $numRowsEvents){ 
            echo ",";
          }
          $ctrEvent++;
        }
        ?>
      ],

      eventClick: function(callEvent , jsEvent, view){

       

       if(callEvent.allow_modal == 1 && (callEvent.user_type == 'A' || callEvent.user_type == 'D')){
           $("#view_event").modal();
       }
       
        $("#event_id1").val(callEvent.id);
        $("#event_name1").html(callEvent.event_name);
        $("#event_date").val(callEvent.event_date);
        $("#event_time").val(callEvent.event_time);
        $("#event_place").val(callEvent.event_place);
        $("#event_description").val(callEvent.event_description);

      },
      eventDragStop: function(event,jsEvent) {
        var event_id = event.id;
          var trashEl = jQuery('#calendarTrash');
          var ofs = trashEl.offset();


          var x1 = ofs.left;
          var x2 = ofs.left + trashEl.outerWidth(true);
          var y1 = ofs.top;
          var y2 = ofs.top + trashEl.outerHeight(true);

         
      },
      editable  : true,
      droppable : false, // this allows things to be dropped onto the calendar !!!
      drop      : function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject')

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject)

        // assign it the date that was reported
        copiedEventObject.start           = date
        copiedEventObject.allDay          = allDay
        copiedEventObject.backgroundColor = $(this).css('background-color')
        copiedEventObject.borderColor     = $(this).css('border-color')

        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove()
        }

      }
    })
  });

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    //Color chooser button
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({
        'background-color': currColor,
        'border-color'    : currColor
      })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      //Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      //Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.html(val)
      $('#external-events').prepend(event)

      //Add draggable funtionality
      ini_events(event)

      //Remove event from text input
      $('#new-event').val('')
    })
  })


function next(){

  $("#btn_prev").css("color","5d5c5c");
  $("#btn_prev").prop("disabled",false);


   var min_date=$("#min_date").val();
    var max_date=$("#max_date").val();
    var event_date_now=$("#event_date_now").val();

    if (event_date_now == max_date){
          $("#btn_next").prop("disabled",true);
          $("#btn_next").css("color","#bfbfbf");

        }else{
          $.post("ajax/getNextEvent.php", {
                event_date_now: event_date_now
            },
            function (data, status) {
                var o = JSON.parse(data);
              
          $("#contact_person").html(o.contact_person);
          $("#contact_num").html(o.contact_num);
          $("#event_place").html(o.event_place);
          $("#event_description").html(o.event_description);
          $("#event_time").html(o.event_time);
          $("#event_date").html(o.event_date);
          $("#event_name").html(o.event_name);
          $("#event_date_now").val(o.event_date);
          $("#event_id").val(o.event_id);
          disabledNow();
          check_present(); 
         
           });
   }
   
}

function prev(){

  $("#btn_next").prop("disabled",false);
  $("#btn_next").css("color","#5d5c5c");

  var min_date=$("#min_date").val();
  var max_date=$("#max_date").val();
  var event_date_now=$("#event_date_now").val();

  if (event_date_now == min_date){
    $("#btn_prev").prop("disabled",true);
    $("#btn_prev").css("color","#bfbfbf");
  }else{
    $.post("ajax/getPrevEvent.php", {
        event_date_now: event_date_now
      },
      function (data, status) {
          var o = JSON.parse(data);
        
        $("#contact_person").html(o.contact_person);
        $("#contact_num").html(o.contact_num);
        $("#event_place").html(o.event_place);
        $("#event_description").html(o.event_description);
        $("#event_time").html(o.event_time);
        $("#event_date").html(o.event_date);
        $("#event_name").html(o.event_name);
        $("#event_date_now").val(o.event_date);
        $("#event_id").val(o.event_id);
        disabledPrev();
        check_present();
     });
  }
}


function nowEvent(){
 
     $.post("ajax/getNowEvent.php", 
            function (data, status) {
                var o = JSON.parse(data);

              
          $("#contact_person").html(o.contact_person);
          $("#contact_num").html(o.contact_num);
          $("#event_place").html(o.event_place);
          $("#event_description").html(o.event_description);
          $("#event_time").html(o.event_time);
          $("#event_date").html(o.event_date);
          $("#event_name").html(o.event_name);
          $("#event_date_now").val(o.event_date);
          $("#event_id").val(o.event_id);

          disabledNow();
          check_present();
     });
}

function disabledNow(){
  var max_date=$("#max_date").val();
  var event_date_now=$("#event_date_now").val();

  if (max_date == event_date_now ){
   $("#btn_next").prop("disabled",true);
    $("#btn_next").css("color","#bfbfbf");
  }
}

function disabledPrev(){
  var min_date=$("#min_date").val();
  var event_date_now=$("#event_date_now").val();

  if (min_date == event_date_now ){
   $("#btn_prev").prop("disabled",true);
    $("#btn_prev").css("color","#bfbfbf");

   
  }
 
}


function present() {
  $("#notif").removeClass("animated fadeOut");

     var event_id = $("#event_id").val();
      $("#btn_present").prop('disabled', true);
      $("#btn_present").html("<span class='fa fa-spinner fa-spin'></span> Loading ...");

     $.ajax({
        type:"POST",
        url:"ajax/check_attendance_student.php",
        data:{
          event_id:event_id
        },
        success:function(data){

        if(data == 1){
          $("#notif").addClass("animated fadeIn");
          $("#notif").html("<span class='alert alert-success'> Successfully Saved. </span>");
        }else if(data == 2){
          $("#notif").addClass("animated fadeIn");
          $("#notif").html("<span class='alert alert-wanring'> You have already Confirmed. </span>");
        }else{
          $("#notif").addClass("animated fadeIn");
          $("#notif").html("<span class='alert alert-danger'> Sorry. Something went wrong. Please try again later. </span>");
        }
      $("#btn_present").prop('disabled', false);
      $("#btn_present").html("<span class='fa fa-check'></span> I'm Going");
        setTimeout(function(){
          $("#notif").removeClass("animated fadeIn");
        $("#notif").addClass("animated fadeOut");
         

       }, 2000);

        check_present();  
        }
      });
     
    }


function check_present() {


      var event_id = $("#event_id").val();
      var user_id = $("#user_id").val();

     $.ajax({
        type:"POST",
        url:"ajax/check_present.php",
        data:{
          event_id:event_id,
          user_id:user_id
        },
        success:function(data){
        if(data == 0){
          var event_date_now = $("#event_date_now").val();
          var date_now = $("#date_now").val();

            if(date_now <= event_date_now){
              $("#list_btn").css("display","block");
            }else{
              $("#list_btn").css("display","none");
            }
        }else{
          $("#list_btn").css("display","none");
        }
        }
      });
     
    }

    function redirect(){
      var event_id = $("#event_id1").val();

      window.location.replace("today_event.php?id="+event_id);
    }

    $(document).ready(function (){
        nowEvent();
    });

</script>
</body>
</html>

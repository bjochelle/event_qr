<style type="text/css">
  #example1_paginate{
    display: none;
  }
  #example1_length{
    display: none;
  }
</style>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Admins</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content" id="show">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
          <div class="pull-right" style="    padding: 20px 20px 0px;">
            <button class="btn btn-primary btn-sm pull-right" onclick="showAddModal()"><span class="fa fa-plus-circle" > </span> Add Admin </button>

               <?php require 'modals/modal_add_admin.php'; ?>
          </div>
            
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Added by</th>
                </tr>
                </thead>
                <tbody>
               
               
                </tbody>
             
              </table>
              <?php require 'modals/modal_response.php'; ?>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>

  <?php include "footer.php";?>

  <script type="text/javascript">
    


function showAddModal(){
  $("#modalAddStudent").modal('show');
}


  $("#add_student").submit(function(e){

    $("#btn_add").prop("disabled",true);
    $("#btn_add").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    e.preventDefault();
    $.ajax({
      url:"ajax/add_user.php",
      method:"POST",
      data:$("#add_student").serialize(),
      success: function(data){
        if(data == 1){
          get_Student();
         success_add();
         $("#add_student")[0].reset();
        }else{

         failed_query();
        }
        $("#modalAddStudent").modal("hide");
        $("#btn_add").prop("disabled",false);
        $("#btn_add").html("<span class='fa fa-check-circle'></span> Save ");
      }
    });
  });





  function get_Student(){
  var table = $('#example1').DataTable();
  table.destroy();
  $("#example1").dataTable({
    "processing":true,
    "ajax":{
      "type":"POST",
      "url":"ajax/datatables/admin_dt.php",
      "dataSrc":"data",
      "lengthMenu": [[-1], ["All"]],

    },
    "columns":[

      {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"username"
      },
      {
        "data":"created_by"
      },

    ]
  });
}

$(document).ready(function(){
  get_Student();
})

</script>
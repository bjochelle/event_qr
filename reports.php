<style type="text/css">
  @media print {
    body {
  font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;
  font-size: 1em;
  color: #333333;
  margin-top: 2cm;
  margin-right: 2cm;
  margin-bottom: 1.5cm;
  margin-left: 2cm
}

  #report{
    margin-top: 10px;
  }
        }
</style>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Attendance Report </h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          
         <div class="card" style="width: 100%;">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-12" style="    padding: 0px;"> 
                <div class="col-md-12 input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><strong>Event Name: <span style="color:red;">*</span></span></strong></span>
                    </div>
                  
                    <select id="event_name" style="text-transform: capitalize;margin-right: 10px;">
                      <option value="">-- Select Event --</option>
                      <?php 

                      include "core/config.php";

                         if($user_type === 'A'){
                               $event = mysql_query("SELECT * from tbl_event  where user_id='$id'");
                         }else{
                               $event = mysql_query("SELECT * from tbl_event ");
                         }
                 
                     while($row = mysql_fetch_array($event)){ ?>
                              <option value="<?php echo $row['event_id'];?>"><?php echo $row['event_name'] ?></option>

                      <?php } ?>
                    </select>
                  
                   <div class="input-group-prepend">
                      <span class="input-group-text"><strong>Filter By: <span style="color:red;">*</span></span></strong></span>
                    </div>
                  
                    <select id="filter" style="text-transform: capitalize;margin-right: 10px;">
                      <option value="1">-- Attended --</option>
                      <option value="0">-- Not Attended --</option>
                     
                    </select>


                  <div class="col-md-3 input-group">
                  

                    <button class="btn btn-primary btn-sm" onclick="gen()" id="btn_gen"><span class="fa fa-refresh"></span> Generate </button>

                    <button class="btn btn-default btn-sm"  onclick="myFunction()" ><span class="fa fa-print"></span> Print </button>
                  </div>
                  
                </div>

                  
                </div>
              </div>
            
              <div class="card-body" id="report">

            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  
  <script src="dist/js/jquery.PrintArea.js"></script>

  <script type="text/javascript">
    function gen() {
        var event_id = $("#event_name").val();
        var status = $("#filter").val();

    if (event_id == ""){
      alert ("Please fill in the form");
    }else{

      $("#btn_gen").prop('disabled', true);
      $("#btn_gen").html("<span class='fa fa-spinner fa-spin'></span> Loading ...");

     $.ajax({
        type:"POST",
        url:"ajax/report.php",
        data:{
          event_id:event_id,
          status:status
        },
        success:function(data){
             $("#report").html(data);
      
          $("#btn_gen").prop('disabled', false);
          $("#btn_gen").html("<span class='fa fa-refresh'></span> Generate");
        }
      });
      }
     
    }

    function myFunction() {
    var mode = 'iframe'; // popup
    var close = mode == "popup";
    var options = { mode : mode, popClose : close};
    $("#report").printArea( options );

}

function getSection(){

  var sy = $("#sy").val();
   $.post("ajax/getSection.php", {
                sy: sy
            },
            function (data) {
              $("#section").html(data);
     });
}
  </script>
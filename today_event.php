<?php 
include "core/config.php";
  	session_start();
	session_destroy();
	$id = $_GET['id'];
$fetch_event = mysql_fetch_array(mysql_query("SELECT * FROM tbl_event where event_id='$id'"));

?>
<!DOCTYPE HTML>
<html>

<head>
	  <title>Eventia</title>

  <link rel="icon" type="image/ico" href="images/images.jpg" />
	<!-- Meta-Tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" >
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	  <link rel="icon" type="image/ico" href="images/logo.png" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link href="dist/css/animate.css" rel='stylesheet' type='text/css'/>
	<!--// Stylesheets -->
	<!--fonts-->
	<!--//fonts-->

	<style type="text/css">
		.w3ls-login label i{
			    color: #ffad01  !important;
		}
		.copy-wthree p{
			color: #ffad01  !important;
		}
		strong{
			    font-weight: bold;
			    float: left;
		}
	</style>
</head>

<body>
	<br><br>
	<div class="col-md-12 row">
		<!-- form starts here -->

			<div class="col-md-4" style="margin-right: 20px;box-shadow: 1px 1px 1px 2px #e0e0db;margin-left: 5%;">
				<img src="ajax/temp/<?php echo $fetch_event['qr_code'].".png";?>">
				<div class="card card-outline">
		              <div class="card-body box-profile">

		                <ul class="list-group list-group-unbordered mb-3">
		                  <li class="list-group-item">
		                    <strong>Event Name</strong> <a class="float-right"> <span id="event_name"> <?php echo $fetch_event['event_name'];?> </span></a>
		                  </li>
		                  <li class="list-group-item">
		                    <strong>Date </strong> <a class="float-right"><span id="event_date"> <?php echo date("F d, Y",strtotime($fetch_event['event_date']));?> </span></a>
		                  </li>
		                  <li class="list-group-item">
		                    <strong>Time </strong> <a class="float-right"><span id="event_time"> <?php echo date("g:i a",strtotime($fetch_event['event_time']));?> </span></a>
		                  </li>
		                  <li class="list-group-item">
		                    <strong>Place</strong> <a class="float-right"><span id="event_place"> <?php echo $fetch_event['event_place'];?> </span></a>
		                  </li>
		                  <li class="list-group-item">
		                    <strong>Description</strong> <a class="float-right"><span id="event_description"> <?php echo $fetch_event['event_description'];?> </span></a>
		                  </li>
		                  <li class="list-group-item">
		                    <strong>Contact Person</strong> <a class="float-right"><span id="contact_person"><?php echo $fetch_event['contact_person'];?> </span></a>
		                  </li>
		                  <li class="list-group-item">
		                    <strong>Contact No.</strong> <a class="float-right"><span id="contact_num"> <?php echo $fetch_event['contact_num'];?> </span></a>
		                  </li>
		                </ul>
		              </div>
		        </div>
			</div>
		
			<div class="col-md-7">

				<div class="card-header">
               		<h3 class="card-title"><strong>Attendee</strong></h3>
              	</div>

				   <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Time</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>


              <div class="card-header">
               		<h3 class="card-title"><strong>Pending</strong></h3>
              	</div>

				   <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
			</div>



			

	</div>
	<!-- //form ends here -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			getPending();
			getAttend();
		});

		 function getPending(){
		 	var event_id = <?php echo $_GET['id'];?>;
				  $('#example1').DataTable({
				    "processing":true,
				    "ajax":{
				      "url":"ajax/datatables/table_today_pending.php",
				      "type":"POST",
				      "data":{
				      	event_id:event_id
				      },
				      "dataSrc":"data"
				    },
				    "columns":[
				      {
				        "data":"count"
				      },
				      {
				        "data":"name"
				      }
				    ]
				  });
		}

		function getAttend(){
		 	var event_id = <?php echo $_GET['id'];?>;
				  $('#example').DataTable({
				    "processing":true,
				    "ajax":{
				      "url":"ajax/datatables/table_today_attend.php",
				      "type":"POST",
				      "data":{
				      	event_id:event_id
				      },
				      "dataSrc":"data"
				    },
				    "columns":[
				      {
				        "data":"count"
				      },
				      {
				        "data":"name"
				      },
				      {
				        "data":"time"
				      }
				    ]
				  });
		}


	</script>
</body>

</html>